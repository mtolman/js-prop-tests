# Property Testing

This library provides a property testing framework for JavaScript and TypeScript, as well as fake data generators (e.g. names, credit card info, etc.). The fake data generators integrate with the property testing framework, that way developers can test their code with realistic data.

Additionally, this library is set up to be used with existing unit test frameworks (e.g. Jest) rather than providing a separate unit test framework. This allows property testing to be integrated with existing test suites.

## Installation

This package is available on [npm](https://www.npmjs.com/package/@matttolman/prop-test).

```bash
npm i -D @matttolman/prop-test 
```

## Environment Variables

* `PROP_TEST_SEED` The seed to use for generating random values
* `PROP_TEST_ITERS` The default number of iterations to run for every property (default: 500)
* `PROP_TEST_DATA_VERSION` The fake data version to use for generating random values

## Usage

The property test framework can be done with any framework which throws an assertion exception when there is a failed assertion. The property test framework will catch those errors, update the message with the sample inputs, automatically perform shrinking, and then rethrow the exception to the wrapping test framework.  

If your framework isn't listed here, you should still be able to use the property test framework just fine. Just do your normal test case declaration process, call `property('name', ...generators).test(() => { your code })` and have your assertions be in the body of the function passed to the `test` call.

### Jest

Jest is pretty straightforward. It works perfectly inside the
`it` test cases you're used to.

```javascript
const {property, generators} = require('@mtolman/prop-test')

describe('Math', () => {
    it('can add', () => {
        const intGen = generators.number.ints({min: -1000, max: 1000})

        property('commutative', intGen, intGen)
            .test((a, b) => {
                expect(a + b).toEqual(b + a)
            })

        property('associative', intGen, intGen, intGen)
            .test((a, b, c) => {
                expect((a + b) + c).toEqual(a + (b + c))
            })

        property('identity', intGen)
            .test((a) => {
                expect(a + 0).toEqual(a)
            })
    })
})
```

### Mocha

Mocha is pretty straightforward. Just use it in the `it` test cases you're used to.

```javascript
const {property, generators} = require('../../../out/prop-test')
const assert = require('assert');

describe('Math', () => {
    it('can add', () => {
        const intGen = generators.number.ints({min: -1000, max: 1000})

        property('commutative', intGen, intGen)
            .test((a, b) => {
                assert.equal(a + b, b + a)
            })

        property('associative', intGen, intGen, intGen)
            .test((a, b, c) => {
                assert.equal((a + b) + c, a + (b + c))
            })

        property('identity', intGen)
            .test((a) => {
                assert.equal(a + 0, a)
            })
    })
})
```
