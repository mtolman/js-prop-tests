const {property, generators} = require('../../out/prop-test')

describe('Math', () => {
    it('can add', () => {
        const intGen = generators.number.ints({min: -1000, max: 1000})

        property('commutative', intGen, intGen)
            .test((a, b) => {
                expect(a + b).toEqual(b + a)
            })

        property('associative', intGen, intGen, intGen)
            .test((a, b, c) => {
                expect((a + b) + c).toEqual(a + (b + c))
            })

        property('identity', intGen)
            .test((a) => {
                expect(a + 0).toEqual(a)
            })
    })
})
