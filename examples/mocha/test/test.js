const {property, generators} = require('../../../out/prop-test')
const assert = require('assert');

describe('Math', () => {
    it('can add', () => {
        const intGen = generators.number.ints({min: -1000, max: 1000})

        property('commutative', intGen, intGen)
            .test((a, b) => {
                assert.equal(a + b, b + a)
            })

        property('associative', intGen, intGen, intGen)
            .test((a, b, c) => {
                assert.equal((a + b) + c, a + (b + c))
            })

        property('identity', intGen)
            .test((a) => {
                assert.equal(a + 0, a)
            })
    })
})
