import csv
import os
import json
import re
from re import sub

versions = [
    {'semVer': '0.0.0', 'key': 0}
]

latestVersion = versions[-1]['key']
versionMap = {
    'latest': str(latestVersion)
}

for version in versions:
    versionMap[version['semVer']] = str(version['key'])


def is_active(targetVersion, version, removedVersion):
    if (int(targetVersion) >= int(version)):
        return removedVersion == "-" or int(targetVersion) < int(removedVersion)
    return False


def organize_by_version(rows, processCols=[]):
    resMap = {}
    for ver in range(latestVersion + 1):
        verList = []
        for row in rows:
            row = process_row(row, processCols)
            if is_active(ver, row['version'], row['removed']):
                del row['version']
                del row['removed']
                for key in row.keys():
                    if row[key] == '-':
                        row[key] = ''
                if len(row.values()) == 1:
                    verList.append(next(iter(row.values())))
                else:
                    verList.append(row)
        resMap[ver] = verList
    return resMap


def process_cell(cell=''):
    return cell.split('|')


def process_row(row={}, cols=[]):
    for col in cols:
        if col in row:
            row[col] = process_cell(row[col])
    return row


def organize_by_col(rows, col, simplifyArray):
    resMap = {}
    for row in rows:
        key = row[col]
        if not key in resMap:
            resMap[key] = []
        resMap[key].append(process_row(row))
    if simplifyArray:
        for key in resMap.keys():
            if len(resMap[key]) == 1:
                resMap[key] = resMap[key][0]
    return resMap


def organize_by_cols_impl(possibleDict, col, simplifyArray):
    if not isinstance(possibleDict, dict):
        return organize_by_col(possibleDict, col, simplifyArray)
    for k, v in possibleDict.items():
        possibleDict[k] = organize_by_cols_impl(v, col, simplifyArray)
    return possibleDict


def organize_by_cols(rows, cols=[], simplifyArray=False, processCols=[]):
    verisonedRows = organize_by_version(rows, processCols=processCols)
    if not bool(cols):
        return verisonedRows

    for ver, rows in verisonedRows.items():
        res = rows
        for col in cols:
            res = organize_by_cols_impl(res, col, simplifyArray)
        verisonedRows[ver] = res
    return verisonedRows


def camel_case(s):
    s = sub(r"(_|-)+", " ", s).title().replace(" ", "")
    return ''.join([s[0].lower(), s[1:]])


def camel_case_keys(dict={}):
    res = {}
    for key in dict.keys():
        res[camel_case(key)] = dict[key]

    return res


def hex_buffer(seq):
    return {"buffer": [int(x, 16) for x in seq.strip().split(' ') if len(x) > 0]}


def hex_seq(seq):
    return int(''.join([x for x in seq.strip().split(' ') if len(x) > 0]), 16)


def hexify(row={}, hexRangeCols=[], hexSeqCols=[]):
    for colKey in hexRangeCols:
        rowVal = []
        sections = [section for section in row[colKey].split('|')]
        for section in sections:
            if '..' in section:
                split = section.split('..')
                rowVal.append({"start": hex_seq(split[0]), "end": hex_seq(split[1])})
            else:
                s = hex_seq(section)
                rowVal.append({"start": s, "end": s})
        row[colKey] = rowVal

    for colKey in hexSeqCols:
        row[colKey] = hex_buffer(row[colKey].strip())
    return row


def read_file(dataDir, fileName, hexRangeCols=[], hexSeqCols=[]):
    res = []
    with(open(os.path.join('faker-data', 'data-generator', dataDir, fileName))) as file:
        reader = csv.DictReader(file)
        for row in reader:
            row = camel_case_keys(row)
            res.append(hexify(row, hexRangeCols, hexSeqCols))
    return res


def read_organized_file(dataDir, fileName, otherCols=[], hexRangeCols=[], hexSeqCols=[], simplifyArray=False,
                        processCols=[]):
    return organize_by_cols(read_file(dataDir, fileName, hexRangeCols, hexSeqCols), otherCols,
                            simplifyArray=simplifyArray, processCols=processCols)


output = {
    'aviation': {
        'aircraftTypes': read_organized_file('aviation', 'aircraft_types.csv'),
        'airlines': read_organized_file('aviation', 'airlines.csv'),
        'airplanes': read_organized_file('aviation', 'airplanes.csv'),
        'airports': read_organized_file('aviation', 'airports.csv'),
    },
    'colors': {
        'colors': read_organized_file('colors', 'colors.csv')
    },
    'computer': {
        'cmakeSystemNames': read_organized_file('computer', 'cmake_system_names.csv'),
        'cpuArchitectures': read_organized_file('computer', 'cpu_architectures.csv'),
        'fileExtensions': read_organized_file('computer', 'file_extensions.csv'),
        'fileMimeMapping': read_organized_file('computer', 'file_mime_mapping.csv'),
        'fileTypes': read_organized_file('computer', 'file_types.csv'),
        'mimeTypes': read_organized_file('computer', 'mime_types.csv'),
        'os': read_organized_file('computer', 'os.csv'),
    },
    'financial': {
        'accountNames': read_organized_file('financial', 'account_names.csv', ['complexity']),
        'creditCardNetworks': read_organized_file('financial', 'cc_networks.csv', [],
                                                  processCols=['starts', 'lengths']),
        'currencies': read_organized_file('financial', 'currencies.csv', []),
        'transactionTypes': read_organized_file('financial', 'transaction_types.csv', []),
    },
    'internet': {
        "topLevelDomains": read_organized_file('internet', 'top_level_domains.csv', []),
        "urlProtocols": read_organized_file('internet', 'url_protocols.csv', []),
    },
    'malicious': {
        "edgeCases": read_organized_file('malicious', 'edge_cases.csv', [], hexSeqCols=['bytes']),
        "formatInjection": read_organized_file('malicious', 'format_injection.csv', []),
        "sqlInjection": read_organized_file('malicious', 'sql_injection.csv', []),
        "xss": read_organized_file('malicious', 'xss.csv', []),
    },
    'person': {
        "givenNames": read_organized_file('person', 'given_names.csv', ['sex', 'complexity']),
        "prefixes": read_organized_file('person', 'prefixes.csv', ['sex', 'complexity']),
        "suffixes": read_organized_file('person', 'suffixes.csv', ['complexity']),
        "surnames": read_organized_file('person', 'surnames.csv', ['complexity']),
    },
    'text': {
        'asciiCharGroups': read_organized_file('text', 'ascii_char_groups.csv', otherCols=['group'],
                                               hexRangeCols=['charactersHexRange'], simplifyArray=True),
        'easciiCharGroups': read_organized_file('text', 'eascii_char_groups.csv', otherCols=['group'],
                                                hexRangeCols=['charactersHexRange'], simplifyArray=True),
        'unicodeCharCategories': read_organized_file('text', 'unicode_char_categories.csv', otherCols=['category'],
                                                     hexRangeCols=['hexRanges'], simplifyArray=True),
    },
    'world': {
        "countries": read_organized_file('world', 'countries.csv'),
        "timezones": read_organized_file('world', 'timezones.csv')
    },
}

jsonEncoder = json.JSONEncoder(indent=2)

bufferRegex = re.compile(r"\"buffer\": \[([^\]]*)\]")


def build_type_for_impl(val):
    if type(val) is dict:
        elems = []
        for key in val.keys():
            elems.append('{key}: {type}'.format(key=key, type=build_type_for_impl(val[key])))
        return '{' + ';'.join(elems) + '}'
    elif type(val) is list:
        if len(val) > 0:
            return build_type_for_impl(val[0]) + '[]'
        return 'any[]'
    elif type(val) is str:
        return 'string'
    else:
        return 'number'


def find_types_impl(val = {}):
    keyType = '[key: string]'
    firstKey = next(iter(val.keys()))
    firstElem = val[firstKey]
    if not type(firstElem) is dict or firstKey in firstElem.values():
        return '{{{key}: {val}}}'.format(key=keyType, val=build_type_for_impl(firstElem))
    else:
        return '{{{key}: {val}}}'.format(key=keyType, val=find_types_impl(firstElem))
    


def build_types_for(val):
    key = '[version: string]'
    versionElem = val[next(iter(val))]
    if type(versionElem) is dict:
        elemType = find_types_impl(versionElem)
    else:
        elemType = build_type_for_impl(versionElem)
    return '{' + key + ':' + elemType + '}'


def js_out(val, types=None):
    return ('const data : {type} = {data};\nexport default data'.format(
        data=bufferRegex.sub('"buffer": Buffer.from(new Uint8Array([\\1]))',
                              jsonEncoder.encode(val)),
        type=(types or build_types_for(val))))


def write_file(targetPath, data, types=None):
    if not os.path.exists(os.path.dirname(targetPath)):
        os.makedirs(os.path.dirname(targetPath))
    with(open(targetPath, 'w')) as outFile:
        outFile.write(js_out(data, types=types))


def write_index_file(dirPath):
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)
    requires = [file for file in os.listdir(dirPath) if file.endswith('.ts') and file != 'index.ts']
    export = 'module.exports = {'
    imports = ''
    for require in requires:
        varName = require.replace('.ts', '')
        imports += 'const {varName} = require("./{varName}");\n'.format(varName=varName)
        export += varName + ','
    export += "};"
    with(open(os.path.join(dirPath, 'index.ts'), 'w')) as outFile:
        outFile.write(imports)
        outFile.write(export)


def write_data_index_file(dirPath):
    if not os.path.exists(dirPath):
        os.mkdir(dirPath)
    requires = [file for file in os.listdir(dirPath) if
                os.path.isdir(os.path.join(dirPath, file)) and os.path.exists(os.path.join(dirPath, file, 'index.ts'))]
    export = 'module.exports = {'
    imports = ''
    for require in requires:
        varName = require.replace('.ts', '')
        imports += 'const {varName} = require("./{varName}");\n'.format(varName=varName)
        export += varName + ','
    export += "};"
    with(open(os.path.join(dirPath, 'index.ts'), 'w')) as outFile:
        outFile.write(imports)
        outFile.write(export)


write_file(os.path.join('src', 'generator', 'aviation', 'data', 'aircraft_types.ts'),
           output['aviation']['aircraftTypes'])
write_file(os.path.join('src', 'generator', 'aviation', 'data', 'airlines.ts'), output['aviation']['airlines'])
write_file(os.path.join('src', 'generator', 'aviation', 'data', 'airplanes.ts'), output['aviation']['airplanes'])
write_file(os.path.join('src', 'generator', 'aviation', 'data', 'airports.ts'), output['aviation']['airports'])

write_file(os.path.join('src', 'generator', 'colors', 'data', 'colors.ts'), output['colors']['colors'])

write_file(os.path.join('src', 'generator', 'computer', 'data', 'cmake_system_names.ts'),
           output['computer']['cmakeSystemNames'])
write_file(os.path.join('src', 'generator', 'computer', 'data', 'cpu_architectures.ts'),
           output['computer']['cpuArchitectures'])
write_file(os.path.join('src', 'generator', 'computer', 'data', 'file_extensions.ts'),
           output['computer']['fileExtensions'])
write_file(os.path.join('src', 'generator', 'computer', 'data', 'file_mime_mapping.ts'),
           output['computer']['fileMimeMapping'])
write_file(os.path.join('src', 'generator', 'computer', 'data', 'file_types.ts'), output['computer']['fileTypes'])
write_file(os.path.join('src', 'generator', 'computer', 'data', 'mime_types.ts'), output['computer']['mimeTypes'])
write_file(os.path.join('src', 'generator', 'computer', 'data', 'os.ts'), output['computer']['os'])

write_file(os.path.join('src', 'generator', 'financial', 'data', 'account_names.ts'),
           output['financial']['accountNames'])
write_file(os.path.join('src', 'generator', 'financial', 'data', 'credit_card_networks.ts'),
           output['financial']['creditCardNetworks'])
write_file(os.path.join('src', 'generator', 'financial', 'data', 'currencies.ts'), output['financial']['currencies'])
write_file(os.path.join('src', 'generator', 'financial', 'data', 'transaction_types.ts'),
           output['financial']['transactionTypes'])

write_file(os.path.join('src', 'generator', 'internet', 'data', 'top_level_domains.ts'),
           output['internet']['topLevelDomains'])
write_file(os.path.join('src', 'generator', 'internet', 'data', 'url_protocols.ts'), output['internet']['urlProtocols'])

write_file(os.path.join('src', 'generator', 'malicious', 'data', 'edge_cases.ts'), output['malicious']['edgeCases'], types='{[version: string]:{name: string;bytes: {buffer: Buffer}}[]}')
write_file(os.path.join('src', 'generator', 'malicious', 'data', 'format_injection.ts'),
           output['malicious']['formatInjection'])
write_file(os.path.join('src', 'generator', 'malicious', 'data', 'sql_injection.ts'),
           output['malicious']['sqlInjection'])
write_file(os.path.join('src', 'generator', 'malicious', 'data', 'xss.ts'), output['malicious']['xss'])

write_file(os.path.join('src', 'generator', 'person', 'data', 'given_names.ts'), output['person']['givenNames'])
write_file(os.path.join('src', 'generator', 'person', 'data', 'prefixes.ts'), output['person']['prefixes'])
write_file(os.path.join('src', 'generator', 'person', 'data', 'suffixes.ts'), output['person']['suffixes'])
write_file(os.path.join('src', 'generator', 'person', 'data', 'surnames.ts'), output['person']['surnames'])

write_file(os.path.join('src', 'generator', 'text', 'data', 'ascii_char_groups.ts'), output['text']['asciiCharGroups'])
write_file(os.path.join('src', 'generator', 'text', 'data', 'eascii_char_groups.ts'),
           output['text']['easciiCharGroups'])
write_file(os.path.join('src', 'generator', 'text', 'data', 'unicode_char_categories.ts'),
           output['text']['unicodeCharCategories'])

write_file(os.path.join('src', 'generator', 'world', 'data', 'countries.ts'), output['world']['countries'])
write_file(os.path.join('src', 'generator', 'world', 'data', 'timezones.ts'), output['world']['timezones'])

write_file(os.path.join('src', 'generator', 'versions', 'data', 'index.ts'), versionMap)
