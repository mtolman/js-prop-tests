/** @type {import('typedoc').TypeDocOptions} */
module.exports = {
    entryPoints: ["./src/prop-test.ts"],
    out: "docs",
    readme: './README.md'
};