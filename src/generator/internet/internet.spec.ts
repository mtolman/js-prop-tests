import {domain, topLevelDomain, url, urlProtocol} from "./index";
import {range} from "../../core";

describe('internet', () => {
    it('tlds', () => {
        range(100).forEach(i => expect(topLevelDomain.create(i)).toMatch(/^[\w\-]+$/))
    })

    it('protocols', () => {
        range(100).forEach(i => expect(urlProtocol.create(i)).toMatch(/^\w+$/))
    })

    it('domain', () => {
        range(100).forEach(i => expect(domain.create(i)).toMatch(/^[\w.\-]+$/))
    })

    it('domain', () => {
        range(100).forEach(i => expect(domain.create(i)).toMatch(/^[\w.\-]+$/))
    })

    it('url', () => {
        const fullRegex = /^(?<protocol>https?|s?ftps?):\/\/(?<creds>(?<username>(?:[a-zA-Z0-9\-.+]|%[a-fA-F0-9]{2})+)(?::(?<password>(?:[a-zA-Z0-9\-.+]|%[a-fA-F0-9]{2})+))?@)?(?<host>(?<ipv6>\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))])|(?<ipv4>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(?<domain>[a-z\-0-9A-Z]+\.)+(?<tld>[a-z\-A-Z]+))(?<port>:\d+)?(?<path>(?:\/([a-zA-Z0-9\-._+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(?<query>\?(?:[a-zA-Z0-9\-._&=+]|%[a-fA-F0-9]{2})+)?(?<hash>#(?:[a-zA-Z0-9\-._&?=\/+]|%[a-fA-F0-9]{2})+)?$/
        const gen = url()
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val).toMatch(fullRegex)
            for (const s of gen.simplify(i)) {
                expect(s).toMatch(fullRegex)
                expect(s).not.toEqual(val)
            }
        })
    })
})