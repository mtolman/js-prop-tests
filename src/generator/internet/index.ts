import {index, ValGen, defaultVersion} from "../common";
import tlds from './data/top_level_domains'
import protocols from './data/url_protocols'
import {domainPiece, hex, urlUnencodedChars, urlUnencodedHashChars} from "../text/ascii";
import {hexStr, random, Random} from "../../core";

/**
 * Generator for IP V4 addresses
 * Example: 127.0.0.1
 */
export const ipv4 : ValGen<string> = {
    create(seed: number): string {
        const rnd = random(seed)
        return `${rnd()%256}.${rnd()%256}.${rnd()%256}.${rnd()%256}`
    },
    simplify(seed: number): Iterable<string> { return []; }
}

/**
 * Generator for IP V6 addresses
 * Examples: ::1, ::ffff:127.0.0.1, 2001:db8:3333:4444:5555:6666:7777:8888
 */
export const ipv6 : ValGen<string> = {
    create(seed: number): string {
        const rnd = random(seed)
        const collapsed: boolean = (rnd() & 0x100) !== 0;
        const useIp4: boolean = !collapsed && (rnd() & 0x100) !== 0;

        if (useIp4) {
            return `::ffff:${ipv4.create(seed)}`
        }

        let res = ''
        const pieces = [
            hexStr(rnd()),
            hexStr(rnd()),
            hexStr(rnd()),
            hexStr(rnd()),
            hexStr(rnd()),
            hexStr(rnd()),
            hexStr(rnd()),
            hexStr(rnd())
        ];

        if (collapsed) {
            const num = rnd() % 6 + 2;
            const offset = rnd() % (8 - num);
            for (let i = 0; i < offset; ++i) {
                if (i != 0) {
                    res += ":";
                }
                res += pieces[i];
            }

            res += "::";

            for (let i = offset + num; i < pieces.length; ++i) {
                if (i != offset + num) {
                    res += ":";
                }
                res += pieces[i];
            }
        } else {
            let first = true;
            for (const e of pieces) {
                if (!first) {
                    res += ":";
                }
                res += e;
                first = false;
            }
        }
        return res;
    },
    simplify(seed: number): Iterable<string> { return []; }

}

function tldFn(dataVersion: string = defaultVersion()) : ValGen<string> {
    return index(tlds, dataVersion)
}

function protocolFn(dataVersion: string = defaultVersion()) : ValGen<string> {
    return index(protocols, dataVersion)
}

function domainFn(dataVersion: string = defaultVersion()) : ValGen<string> {
    const maxLen = 63;
    return {
        create(seed: number): string {
            let str = ''
            const rnd = random(seed);
            const tld = tldFn(dataVersion).create(rnd());
            const remaining = Math.max(1, (maxLen - tld.length) >>> 0);
            const maxDomainPieces = Math.max(1, remaining / 6);
            const numDomainPieces = Math.max(1, seed % maxDomainPieces);
            const  maxDomainPieceSize = (remaining - numDomainPieces) / numDomainPieces;
            const domainPieceGen = domainPiece({minLen: 1, maxLen: maxDomainPieceSize});

            for (let sd = 0; sd < numDomainPieces; ++sd) {
                str += domainPieceGen.create(rnd()) + ".";
            }
            str += tld;
            return str
        }, simplify(seed: number): Iterable<string> { return []; }
    }
}

/**
 * Options for URL generation
 */
export interface UrlOptions {
    /**
     * Whether to allow IP V4 addresses for the host
     * Default: true
     */
    allowIpv4Host?: boolean

    /**
     * Whether to allow IP V6 addresses for the host
     * Default: true
     */
    allowIpv6Host?: boolean

    /**
     * Whether to allow encoded characters
     * Default: true
     */
    allowEncodedChars?: boolean

    /**
     * Whether to allow the hash segment (Example: #heading-1)
     * Default: true
     */
    allowHash?: boolean

    /**
     * Whether to allow randomized paths (if not provided, will just use '/')
     * Default: true
     */
    allowRandomPath?: boolean

    /**
     * Whether to allow the username segment (Example: bob@example.com)
     * Default: true
     */
    allowUsername?: boolean

    /**
     * Whether to allow the password segment (Example: bob:pwd@example.com)
     * Default: true
     *
     * **Note:** The password segment only appears if allowUsername is also true
     */
    allowPassword?: boolean

    /**
     * Whether to allow the query segment (Example: ?param=value)
     * Default: true
     */
    allowQuery?: boolean

    /**
     * Whether to allow the port
     * Default: true
     */
    allowPort?: boolean

    /**
     * List of allowed schemas. Defaults to the current data set version of protocols
     */
    allowedSchemas?: string[]
}

const defaultUrlOptions = {
    allowIpv4Host: true,
    allowIpv6Host: true,
    allowEncodedChars: true,
    allowHash: true,
    allowRandomPath: true,
    allowUsername: true,
    allowPassword: true,
    allowQuery: true,
    allowPort: true
}

interface ResolvedUrlOptions {
    allowIpv4Host: boolean
    allowIpv6Host: boolean
    allowEncodedChars: boolean
    allowHash: boolean
    allowRandomPath: boolean
    allowUsername: boolean
    allowPassword: boolean
    allowQuery: boolean
    allowPort: boolean
    allowedSchemas: string[]
}

function urlCharacter(allowEncodedChars: boolean, rnd: Random) {
    if (allowEncodedChars && rnd() & 0x10) {
        return "%" + hex({minLen: 2, maxLen: 2}).create(rnd());
    }
    else {
        return urlUnencodedChars({minLen: 1, maxLen: 1}).create(rnd());
    }
}

function urlCharacters(allowEncodedChars: boolean, maxSize: number, seed: number) {
    const rnd = random(seed)
    let str = ''
    const size = (rnd() % maxSize) + 1;
    for (let i = 0; i < size; ++i) {
        str += urlCharacter(allowEncodedChars, rnd);
    }
    return str;
}

function hashCharacter(allowEncodedChars: boolean, rnd: Random) {
    if (allowEncodedChars && rnd() & 0x10) {
        return "%" + hex({minLen: 2, maxLen: 2}).create(rnd());
    }
    else {
        return urlUnencodedHashChars({minLen: 1, maxLen: 1}).create(rnd());
    }
}

function urlHashCharacters(allowEncodedChars: boolean, maxSize: number, seed: number) {
    const rnd = random(seed)
    let str = ''
    const size = (rnd() % maxSize) + 1;
    for (let i = 0; i < size; ++i) {
        str += hashCharacter(allowEncodedChars, rnd);
    }
    return str;
}

function urlHost(options: ResolvedUrlOptions, dataVersion: string, seed: number) {
    const rnd = random(seed);
    const branch = rnd();
    let str = ''
    if (options.allowIpv4Host && branch & 0x00100) {
        str += ipv4.create(rnd())
    }
    else if (options.allowIpv6Host && branch & 0x01000) {
        str += "[" + ipv6.create(rnd()) + "]";
    }
    else {
        str += domainFn(dataVersion).create(rnd());
    }

    if (options.allowPort && branch & 0x00400) {
        str += ":" + (rnd() % 65536);
    }
    return str
}

function urlPath(options: ResolvedUrlOptions, dataVersion: string, seed: number) {
    const rnd = random(seed)
    if (options.allowRandomPath && (rnd() & 0x30) != 0) {
        if ((rnd() & 0x10) != 0) {
            return '/'
        }

        const numSegments = rnd() % 48;

        let str = ''
        str += "/";

        for (let i = 0; i < numSegments; ++i) {
            if (i != 0) {
                str += "/";
            }
            str += urlCharacters(options.allowEncodedChars, 15, rnd());

            // optional trailing slash
            if (i + 1 == numSegments && (rnd() & 0x1000) != 0) {
                str += "/";
            }
        }
        return str;
    }
    return null;
}

function urlQuery(options: ResolvedUrlOptions, dataVersion: string, seed: number) {
    const rnd = random(seed)

    if (!options.allowQuery || rnd() & 0x00100) {
        return null
    }

    let str = '?'
    const numPieces = rnd() % 32 + 1;

    for (let i = 0; i < numPieces; ++i) {
        if (i != 0) {
            str += "&";
        }

        str += urlCharacters(options.allowEncodedChars, 25, rnd());

        if (rnd() & 0x100) {
            str += "=";
            str += urlCharacters(options.allowEncodedChars, 25, rnd());
        }
    }
    return str
}

function urlHash(options: ResolvedUrlOptions, dataVersion: string, seed: number) {
    const rnd = random(seed)

    if (!options.allowHash || rnd() & 0x00100) {
        return;
    }

    return "#" + urlHashCharacters(options.allowEncodedChars, 25, rnd());
}

function urlFn(dataVersion: string) : (opts?: UrlOptions) => ValGen<string> {
    return function impl(opts: UrlOptions = {}): ValGen<string> {
        const options: ResolvedUrlOptions = {allowedSchemas: protocols[dataVersion], ...defaultUrlOptions, ...opts}
        const create = (seed: number): string => {
            const rnd = random(seed)
            let str = ''

            // We can use `rnd()` here and still be able to safely reduce since both paths call `rnd()`
            if (options.allowedSchemas.length == 0) {
                str += protocolFn(dataVersion).create(rnd());
            }
            else {
                str += options.allowedSchemas[rnd() % options.allowedSchemas.length];
            }

            str += "://";

            const branches = rnd();
            if (options.allowUsername && branches & 0x010) {
                str += urlCharacters(options.allowEncodedChars, 25, rnd() ^ 0x48271932);
                if (options.allowPassword && branches & 0x020) {
                    str += ":" + urlCharacters(options.allowEncodedChars, 25, rnd() ^ 0x12948302);
                }
                else {
                    // Keep our rnd calls balanced
                    const _ = rnd();
                }
                str += "@";
            }
            else {
                // Keep our rnd calls balanced
                const _ = rnd();
                const _1 = rnd();
            }

            str += urlHost(options, dataVersion, rnd() ^ 0x847293f3);
            let path = urlPath(options, dataVersion, rnd() ^ 0x75129382);
            const query = urlQuery(options, dataVersion, rnd() ^ 0x12345678);
            const hash = urlHash(options, dataVersion, rnd() ^ 0x91828374);

            path = path || (query || hash ? '/' : '')

            return str + path + (query || '') + (hash || '');
        }
        return {
            create,
            simplify: function*(seed: number) {
                let optsClone = {...opts}
                if (optsClone.allowPassword) {
                    optsClone = {...optsClone, allowPassword: false}
                    yield impl(optsClone).create(seed)
                }
                if (optsClone.allowUsername) {
                    optsClone = {...optsClone, allowUsername: false}
                    yield impl(optsClone).create(seed)
                }
                if (optsClone.allowIpv4Host) {
                    optsClone = {...optsClone, allowIpv4Host: false}
                    yield impl(optsClone).create(seed)
                }
                if (optsClone.allowIpv6Host) {
                    optsClone = {...optsClone, allowIpv6Host: false}
                    yield impl(optsClone).create(seed)
                }
                if (optsClone.allowPort) {
                    optsClone = {...optsClone, allowPort: false}
                    yield impl(optsClone).create(seed)
                }
                if (optsClone.allowEncodedChars) {
                    optsClone = {...optsClone, allowEncodedChars: false}
                    yield impl(optsClone).create(seed)
                }
                if (optsClone.allowHash) {
                    optsClone = {...optsClone, allowHash: false}
                    yield impl(optsClone).create(seed)
                }
                if (optsClone.allowQuery) {
                    optsClone = {...optsClone, allowQuery: false}
                    yield impl(optsClone).create(seed)
                }
            }
        }
    }
}

/**
 * Creates locked versions of version-dependent generators
 * @param version Version to lock to
 */
export function withVersion(version: string) {
    return {
        topLevelDomain: tldFn(version),
        urlProtocol: protocolFn(version),
        domain: domainFn(version),
        url: urlFn(version)
    }
}

const res = withVersion(defaultVersion())

/**
 * Generator for top-level domains
 * Uses latest data version
 */
export const topLevelDomain = res.topLevelDomain

/**
 * Generator for URL protocols
 * Uses latest data version
 */
export const urlProtocol =  res.urlProtocol

/**
 * Generator for domains
 * Uses latest data version
 */
export const domain = res.domain

/**
 * Generator for URLs domains
 * Uses latest data version
 */
export const url = res.url
