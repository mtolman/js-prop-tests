import {index, map, ValGen, defaultVersion} from "../common";
import countryData from './data/countries'
import timezoneData from './data/timezones'

/**
 * Interface for a country
 */
export interface Country {
    /**
     * Name of the country
     */
    name: string

    /**
     * ISO Alpha-2 code
     */
    alpha2?: string

    /**
     * ISO Alpha-3 code
     */
    alpha3?: string

    /**
     * GENC code
     */
    genc?: string

    /**
     * Stanag code
     */
    stanag?: string

    /**
     * ISO Numeric code
     */
    numericCode?: string

    /**
     * Top level domain
     */
    domain?: string
}

/**
 * Version lockable generators
 */
export interface WorldGenerators {
    /**
     * Country generator
     */
    country: ValGen<Country>

    /**
     * Timezone generator (e.g. America/Denver)
     */
    timezone: ValGen<string>
}

/**
 * Version lock generators
 * @param dataVersion Version to lock to
 */
export function withVersion(dataVersion: string) : WorldGenerators {
    return {
        country: map(
            index<{
                name: string,
                genc: string,
                isoAlpha2: string,
                isoAlpha3: string,
                isoNumeric: string,
                stanag: string,
                internet: string
            }>(countryData, dataVersion),
            (country) => {
                const res: Country = {name: country.name}

                country.isoAlpha2 && country.isoAlpha2 && (res.alpha2 = country.isoAlpha2)
                country.isoAlpha3 && country.isoAlpha3 && (res.alpha3 = country.isoAlpha3)
                country.isoNumeric && country.isoNumeric && (res.numericCode = country.isoNumeric)
                country.genc && country.genc && (res.genc = country.genc)
                country.stanag && country.stanag && (res.stanag = country.stanag)
                country.internet && country.internet && (res.domain = country.internet)

                return res
            }
        ),
        timezone: index(timezoneData, dataVersion)
    }
}

const res = withVersion(defaultVersion())

/**
 * Country generator
 */
export const country = res.country

/**
 * Timezone generator (e.g. America/Denver)
 */
export const timezone = res.timezone
