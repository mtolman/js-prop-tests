import {country, timezone} from "./index";
import {range} from "../../core";

describe('world', () => {
    it('country', () => {
        range(100).forEach(i => {
            expect(country.create(i).name).toBeTruthy()
        })
    })

    it('timezone', () => {
        range(100).forEach(i => {
            expect(timezone.create(i)).toMatch(/[\w\-\s]+\/[\w\-\s]+/)
        })
    })
})
