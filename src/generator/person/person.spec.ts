import {fullName, givenName, person, prefix, suffix, surname} from "./index";
import {range} from "../../core";

describe('person', () => {
    it('person', () => {
        range(100).forEach(i => {
            const p = person().create(i)
            expect(p.fullName).toContain(p.givenName)
            expect(p.fullName).toContain(p.surname)
            expect(p.givenName).toBeTruthy()
            expect(p.surname).toBeTruthy()
            expect(p.sex).toMatch(/^(?:MALE|FEMALE)$/)
        })
    })

    it('givenName', () => {
        range(100).forEach(i => {
            expect(givenName().create(i)).toBeTruthy()
        })
    })

    it('surname', () => {
        range(100).forEach(i => {
            expect(surname().create(i)).toBeTruthy()
        })
    })

    it('fullName', () => {
        range(100).forEach(i => {
            expect(fullName().create(i)).toBeTruthy()
        })
    })

    it('suffix', () => {
        range(100).forEach(i => {
            const v = suffix().create(i)
            if (v) {
                expect(v).toBeTruthy()
            }
            else {
                expect(v).toBeNull()
            }
        })
    })

    it('prefix', () => {
        range(100).forEach(i => {
            const v = prefix().create(i)
            if (v) {
                expect(v).toBeTruthy()
            }
            else {
                expect(v).toBeNull()
            }
        })
    })
})
