import {complexity, DataComplexity, dataComplexityToKey, defaultVersion, map, recover, ValGen} from "../common";
import givenNames from './data/given_names'
import prefixes from './data/prefixes'
import suffixes from './data/suffixes'
import surnames from './data/surnames'
import {random} from "../../core";

/**
 * The sex (male/female) of a person
 */
export enum Sex {
    MALE,
    FEMALE
}

const fullNameFn = (dataVersion: string) => function (
    minComplexity: DataComplexity = DataComplexity.RUDIMENTARY,
    maxComplexity: DataComplexity = DataComplexity.COMPLEX,
    sex?: Sex
): ValGen<string> {
    const givenGen = givenNameFn(dataVersion)(minComplexity, maxComplexity, sex)
    const surGen = surnameFn(dataVersion)(minComplexity, maxComplexity)
    return {
        create(seed: number): string {
            const rnd = random(seed)
            const givenName = givenGen.create(rnd())
            const surname = surGen.create(rnd())
            if (rnd() % 11 == 1) {
                return surname + ' ' + givenName
            }
            else {
                return givenName + ' ' + surname
            }
        }, simplify(_seed: number): Iterable<string> { return []; }
    }
}

const givenNameFn = (dataVersion: string) => function (
    minComplexity: DataComplexity = DataComplexity.RUDIMENTARY,
    maxComplexity: DataComplexity = DataComplexity.COMPLEX,
    sex?: Sex
): ValGen<string> {
    return sexGenerator(dataVersion, minComplexity, maxComplexity, givenNames, sex)
}

const prefixFn = (dataVersion: string) => function (
    minComplexity: DataComplexity = DataComplexity.RUDIMENTARY,
    maxComplexity: DataComplexity = DataComplexity.COMPLEX,
    sex?: Sex
): ValGen<string|null> {
    return recover(
        sexGenerator(dataVersion, minComplexity, maxComplexity, prefixes, sex) as any as ValGen<string|null>,
        (_v) => null
    )
}

const suffixFn = (dataVersion: string) => function (
    minComplexity: DataComplexity = DataComplexity.RUDIMENTARY,
    maxComplexity: DataComplexity = DataComplexity.COMPLEX
): ValGen<string|null> {
    return recover(
        map(
            complexity(suffixes, minComplexity, maxComplexity, dataVersion),
            ({name}) => name
        ),
        (_v) => null
    )
}

const surnameFn = (dataVersion: string) => function (
    minComplexity: DataComplexity = DataComplexity.RUDIMENTARY,
    maxComplexity: DataComplexity = DataComplexity.COMPLEX
): ValGen<string> {
    return map(
        complexity(surnames, minComplexity, maxComplexity, dataVersion),
        ({name}) => name
    )
}

/**
 * Represents a person's data
 */
export interface Person {
    /**
     * The given name of a person
     */
    givenName: string

    /**
     * The surname of a person
     */
    surname: string

    /**
     * The full name of a person (could be `given surname` or `surname given`)
     * Does NOT include prefix or suffix
     */
    fullName: string

    /**
     * The suffix for the person (if available)
     */
    suffix: string|null

    /**
     * The prefix for the person (if available)
     */
    prefix: string|null

    /**
     * The sex of the person
     */
    sex: string
}

const personFn = (dataVersion: string) => function (
    minComplexity: DataComplexity = DataComplexity.RUDIMENTARY,
    maxComplexity: DataComplexity = DataComplexity.COMPLEX
): ValGen<Person> {
    return {
        create(seed: number): Person {
            const rnd = random(seed)
            const sex = seed % 2 as Sex
            seed >>>= 1
            const fname = givenNameFn(dataVersion)(minComplexity, maxComplexity, sex).create(rnd())
            const lname = surnameFn(dataVersion)(minComplexity, maxComplexity).create(rnd())
            const diff = (maxComplexity - minComplexity + 1)
            const complexity = (Math.abs(seed) % diff + minComplexity) as any as DataComplexity

            let fullName = `${fname} ${lname}`
            if (complexity >= DataComplexity.ADVANCED && rnd() % 4 == 1) {
                fullName = `${lname} ${fname}`
            }
            const s = (complexity >= DataComplexity.BASIC && rnd() % 4 == 1) ?
                suffixFn(dataVersion)(minComplexity, maxComplexity).create(rnd()) : null
            const p = (complexity >= DataComplexity.BASIC && rnd() % 5 == 1)
                ? prefixFn(dataVersion)(minComplexity, maxComplexity, sex).create(rnd()) : null
            return {
                givenName: fname,
                surname: lname,
                fullName,
                suffix: s,
                prefix: p,
                sex: sexToKey(sex)
            }
        },
        simplify(_seed: number): Iterable<Person> {return []}
    }
}

/**
 * Interface for version lockable generators
 */
export interface PersonGenerators {
    /**
     * Creates a new person
     * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
     * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
     */
    person(
        minComplexity?: DataComplexity,
        maxComplexity?: DataComplexity
    ): ValGen<Person>

    /**
     * Creates a new name prefix (or a null prefix)
     * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
     * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
     */
    prefix(
        minComplexity?: DataComplexity,
        maxComplexity?: DataComplexity
    ): ValGen<string|null>

    /**
     * Creates a new name suffix (or a null suffix)
     * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
     * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
     */
    suffix(
        minComplexity?: DataComplexity,
        maxComplexity?: DataComplexity
    ): ValGen<string|null>

    /**
     * Creates a new given name
     * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
     * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
     */
    givenName(
        minComplexity?: DataComplexity,
        maxComplexity?: DataComplexity
    ): ValGen<string>

    /**
     * Creates a new surname
     * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
     * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
     */
    surname(
        minComplexity?: DataComplexity,
        maxComplexity?: DataComplexity
    ): ValGen<string>

    /**
     * Creates a new full name
     * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
     * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
     */
    fullName(
        minComplexity?: DataComplexity,
        maxComplexity?: DataComplexity
    ): ValGen<string>
}

/**
 * Version locks generators
 * @param dataVersion Data version to lock to
 */
export function withVersion(dataVersion: string) : PersonGenerators {
    return {
        person: personFn(dataVersion),
        prefix: prefixFn(dataVersion),
        suffix: suffixFn(dataVersion),
        givenName: givenNameFn(dataVersion),
        surname: surnameFn(dataVersion),
        fullName: fullNameFn(dataVersion)
    }
}

const res = withVersion(defaultVersion())

/**
 * Creates a new person
 * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
 * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
 */
export const person = res.person

/**
 * Creates a new name prefix (or a null prefix)
 * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
 * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
 */
export const prefix = res.prefix

/**
 * Creates a new name suffix (or a null suffix)
 * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
 * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
 */
export const suffix = res.suffix

/**
 * Creates a new given name
 * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
 * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
 */
export const givenName = res.givenName

/**
 * Creates a new surname
 * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
 * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
 */
export const surname = res.surname

/**
 * Creates a new full name
 * @param minComplexity Minimum complexity for the names. Default: DataComplexity.RUDIMENTARY
 * @param maxComplexity Maximum complexity for the names. Default: DataComplexity.COMPLEX
 */
export const fullName = res.fullName

function sexToKey(sex: Sex): string {
    switch (sex) {
        case Sex.MALE:
            return 'MALE'
        case Sex.FEMALE:
            return 'FEMALE'
    }
    throw 'Unknown sex ' + sex
}

function sexGenerator(
    dataVersion : string,
    minComplexity: DataComplexity,
    maxComplexity: DataComplexity,
    data: {[version: string]:{[sex: string]: {[complexity: string]: {name: string;sex: string;complexity: string}[]}}},
    sex?: Sex
): ValGen<string> {
    return {
        create(seed: number): string {
            const sexKey = sex ? sexToKey(sex) : sexToKey(seed % 2 as any)
            seed >>>= 1
            const diff = (maxComplexity - minComplexity + 1)
            const complexity = (Math.abs(seed) % diff + minComplexity) as any as DataComplexity
            seed = (seed / diff) | 0
            const dataComplex = data[dataVersion][sexKey][dataComplexityToKey(complexity)]
            return dataComplex[Math.abs(seed) % dataComplex.length].name
        },
        simplify: function*(_seed: number): Iterable<string> {}
    }
}
