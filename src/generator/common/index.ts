import versions from '../versions/data'
import {random, zip} from "../../core";
const {latest} = versions

export function defaultVersion() {
    let version = process.env.PROP_TEST_DATA_VERSION
    if (version && version in versions) {
        version = versions[version]
    }
    else if (version && `${+version}` === version) {
        version = `${+version}`
    }
    else {
        version = latest
    }
    return version;
}

/**
 * Represents a value generator
 * @template T Output value from the generator
 */
export interface ValGen<T> {
    /**
     * Creates a new "random" value
     * @param seed seed to use for value generation
     */
    create(seed: number): T;
    /**
     * Simplifies a random value at a given seed
     * Will return an iterator with all possible simplifications (may be empty if there are no simplifications)
     * @param seed seed to use for value generation and simplification
     */
    simplify(seed: number): Iterable<T>
}

export enum DataComplexity {
    RUDIMENTARY,
    BASIC,
    INTERMEDIATE,
    ADVANCED,
    COMPLEX
}

/**
 * Converts data complexity enum to a string key
 * @param complexity
 */
export function dataComplexityToKey(complexity: DataComplexity) : string {
    switch(complexity) {
        case DataComplexity.RUDIMENTARY:
            return 'RUDIMENTARY'
        case DataComplexity.BASIC:
            return 'BASIC'
        case DataComplexity.INTERMEDIATE:
            return 'INTERMEDIATE'
        case DataComplexity.ADVANCED:
            return 'ADVANCED'
        case DataComplexity.COMPLEX:
            return 'COMPLEX'
    }
    throw new Error('Undefined data complexity level ' + complexity)
}

/**
 * Creates a generator that generates values from a versioned data set
 * @param data Data set to use for creating values
 * @param dataVersion Version for the data set
 */
export function index<T>(data: {[version: string]: T[]}, dataVersion = defaultVersion()) : ValGen<T> {
    return {
        create: function (seed) {
            return data[dataVersion][Math.abs(seed) % data[dataVersion].length]
        },
        simplify: function* (seed) {}
    }
}

/**
 * Generator which takes in input generator and an error handler
 * When the input generator throws, the input handler is called and the result is returned instead
 * @param gen
 * @param errProvider
 */
export function recover<T>(gen: ValGen<T>, errProvider: (err: any) => T) : ValGen<T> {
    return {
        create(seed: number): T {
            try {
                return gen.create(seed)
            } catch (e) {
                return errProvider(e)
            }
        }, simplify: function*(seed: number) {
            try {
                yield* gen.simplify(seed)
            }
            catch (e) {
                yield errProvider(e)
            }
        }
    }
}

/**
 * Creates a generator that generates values from a versioned data set with different complexity levels
 * @param data Data set to use for creating values
 * @param minComplexity Minimum data complexity level
 * @param maxComplexity Maximum data complexity level
 * @param dataVersion Version for the data set
 */
export function complexity<T>(
    data: {[version: string]: {[complexity: string]: T[]}},
    minComplexity: DataComplexity = DataComplexity.RUDIMENTARY,
    maxComplexity: DataComplexity = DataComplexity.COMPLEX,
    dataVersion = defaultVersion()
) : ValGen<T> {
    if (maxComplexity < minComplexity) {
        const tmp = maxComplexity
        maxComplexity = minComplexity
        minComplexity = maxComplexity
    }

    return {
        create: function (seed) {
            const diff = (maxComplexity - minComplexity + 1)
            const complexity = Math.round(seed % diff + minComplexity) as any as DataComplexity
            seed = (seed / diff) | 0
            const dataComplex = data[dataVersion][dataComplexityToKey(complexity)]
            return dataComplex[Math.abs(seed) % dataComplex.length]
        },
        simplify: function* (seed) {}
    }
}

/**
 * Creates a generator which transforms the output of another generator
 * @param inputGenerator Input generator to transform
 * @param mapFunc Transformation function
 */
export function map<T, R>(inputGenerator: ValGen<T>, mapFunc: (_: T, seed?: number) => R) : ValGen<R> {
    return {
        create(seed: number): R {
            return mapFunc(inputGenerator.create(seed), seed)
        },
        simplify: function*(_seed: number){}
    }
}

/**
 * Creates a generator which filters the output of another generator
 * @param inputGenerator Input generator to filter
 * @param filterFunc Filter function
 */
export function filter<T>(inputGenerator: ValGen<T>, filterFunc: (_: T) => boolean) : ValGen<T> {
    return {
        create(seed: number): T {
            let index = seed
            let res = inputGenerator.create(index)
            for (;!filterFunc(res); ++index, res = inputGenerator.create(index)){}
            return res
        },
        simplify: function*(seed: number){
            for (const simplification of inputGenerator.simplify(seed)) {
                if (filterFunc(simplification)) {
                    yield simplification
                }
            }
        }
    }
}

/**
 * Does a simplification search through multiple generators
 * Note: this assumes that individually seeds are pseudorandom generated based off the parameter seed
 * @param rootSeed Seed to simplify
 * @param generators Generators to search through
 */
function* simplifyGenerators(rootSeed: number, generators: ValGen<any>[]) {
    const rnd = random(rootSeed)
    const seeds = [...generators.map(_ => rnd())]
    let curValues = [...generators.map((g, i) => g.create(seeds[i]))]
    const iters: (Iterator<any>|null)[] = [
        ...generators.map(
            (g, i) => (
                (g.simplify(seeds[i])) as Iterable<any>
            )[Symbol.iterator]()
        )
    ]
    let foundValue = false
    do {
        foundValue = false
        for (let i = 0; i < iters.length; ++i) {
            const iter: Iterator<any>|null = iters[i]
            if (iter === null) {
                continue
            }
            const res = iter.next()
            if (!res.done) {
                curValues[i] = res.value
                yield [...curValues]
                foundValue = true
            }
        }
    } while(foundValue)
}

/**
 * Combines multiple generators to create a single object
 * Also provides simplification of the objects, and it randomizes the input generators to cycle through complex combinations
 * @param gens A key/value mapping where keys are the resulting object keys and values are the generators to create the resulting object values
 */
export function combine(gens: {[key: string]: ValGen<any>}): ValGen<{[key: string]: any}> {
    const mappings = Object.entries(gens)
    return {
        create(seed: number): { [p: string]: any } {
            const rnd = random(seed)
            return Object.fromEntries(mappings.map(([k, g]) => {
                return [k, g.create(rnd())]
            }));
        }, simplify: function*(seed: number) {
            const keys = mappings.map(([key, _]) => key)
            const gens = mappings.map(([_, v]) => v)
            const iter = simplifyGenerators(seed, gens)
            for (const valSet of iter) {
                const entries = [...zip(keys, valSet)]
                yield Object.fromEntries(entries)
            }
        }
    }
}

/**
 * Combines multiple generators to create a single tuple
 * Also provides simplification of the objects, and it randomizes the input generators to cycle through complex combinations
 * @param gens A key/value mapping where keys are the resulting object keys and values are the generators to create the resulting object values
 */
export function tuple(...gens: ValGen<any>[]) {
    return {
        create(seed: number): any[] {
            const rnd = random(seed)
            return gens.map(g => g.create(rnd()))
        },
        simplify: function*(seed: number) {
            yield* simplifyGenerators(seed, gens)
        }
    }
}

/**
 * Generator which returns just a single value
 * Does not simplify
 * @param val Value to return
 */
export function just<T>(val: T) : ValGen<T> {
    return {
        create(_seed: number) { return val },
        simplify: function*(_seed: number) {}
    }
}

/**
 * Generator which returns a random value from one of several generators
 * @param gens Available generators to use
 */
export function oneOf<T>(...gens: ValGen<T>[]) : ValGen<T> {
    return {
        create(seed: number) {
            return gens[seed % gens.length].create(seed / gens.length)
        },
        simplify: function*(seed: number) {
            yield* gens[seed % gens.length].simplify(seed / gens.length)
        }
    }
}

/**
 * Options for array generation
 */
export interface ArrayOptions {
    minLen?: number,
    maxLen?: number
}

const defaultArrayOpts = {
    minLen: 0,
    maxLen: 1024
}


/**
 * Generator for an array of data
 * @param gen Available generators to use
 * @param options Options for array generation
 */
export function array<T>(gen: ValGen<T>, options: ArrayOptions = {}) : ValGen<T[]> {
    const opts = {...defaultArrayOpts, ...options}
    function create(seed: number): T[] {
        const rnd = random(seed)
        return (new Array((rnd() % (opts.maxLen - opts.minLen)) + opts.minLen))
            .fill(null)
            .map(() => gen.create(rnd()))
    }

    return {
        create,
        simplify: function*(seed: number) {
            let arr = create(seed)
            const rnd = random(seed)
            while(arr.length > 0) {
                const sliceAmt = (rnd() % 3)
                arr = arr.slice(0, -sliceAmt)
                yield arr
            }
        }
    }
}
