import {
    array,
    combine,
    complexity,
    DataComplexity,
    dataComplexityToKey, defaultVersion,
    filter,
    index,
    just,
    map, oneOf,
    recover, tuple
} from "./index";
import {ints} from "../number";
import {alpha} from "../text/ascii";
import {limit, range} from "../../core";

describe('combine', () => {
    it('generates', () => {
        expect(combine({hello: just('world'), a: just(5)}).create(1)).toEqual({hello: 'world', a: 5})

        const gen = combine({hello: ints({min: -10, max: 10}), a: alpha()})
        range(100).forEach(i => {
            const v = gen.create(i)
            expect(v.hello).toBeGreaterThanOrEqual(-10)
            expect(v.hello).toBeLessThanOrEqual(10)
            expect(v.a).toMatch(/^[a-zA-Z]+$/)
        })
    })

    it ('shrinks', () => {
        const seed = 13
        const gen = combine({hello: ints({min: -10, max: 10}), a: ints({min: 10, max: 30})})
        const iter = limit(100, gen.simplify(seed))
        for (const val of iter) {
            if (val === null) {
                break
            }
            expect(val.hello).toBeGreaterThanOrEqual(-10)
            expect(val.hello).toBeLessThanOrEqual(10)
            expect(val.a).toBeGreaterThanOrEqual(10)
            expect(val.a).toBeLessThanOrEqual(30)
        }
    })
})

describe('dataComplexityToKey', () => {
    it('converts', () => {
        expect(dataComplexityToKey(DataComplexity.RUDIMENTARY)).toEqual('RUDIMENTARY')
        expect(dataComplexityToKey(DataComplexity.BASIC)).toEqual('BASIC')
        expect(dataComplexityToKey(DataComplexity.INTERMEDIATE)).toEqual('INTERMEDIATE')
        expect(dataComplexityToKey(DataComplexity.ADVANCED)).toEqual('ADVANCED')
        expect(dataComplexityToKey(DataComplexity.COMPLEX)).toEqual('COMPLEX')
    })
})

describe('index', () => {
    const data = {'0': ['A', 'D', 'E']}
    const gen = index(data)

    it('creates', () => {
        range(100).forEach(i => expect(gen.create(i)).toMatch(/^[ADE]$/))
    })

    it('does not simplify', () => {
        range(100).forEach(i => expect([...gen.simplify(i)].length).toBe(0))
    })
})

describe('recover', () => {
    const gen = recover(
        filter(
            ints({min: 0, max: 50}),
            (i) => {
                if (i % 13 == 0) throw 'Found unlucky factor'
                return true
            }
        ),
        (_) => -13
    )

    it('creates', () => {
        range(100).forEach(i => {
            const v = gen.create(i)
            if (v < 0) {
                expect(v).toBe(-13)
            }
            else {
                expect(v).toBeGreaterThanOrEqual(0)
                expect(v).toBeLessThanOrEqual(50)
            }
        })
    })

    it('does simplify (sort of)y', () => {
        expect([...gen.simplify(34)]).not.toEqual([])
    })
})

describe('complexity', () => {
    const data = {'0': {'RUDIMENTARY': ['A', 'D', 'E']}}
    const gen = complexity(data, DataComplexity.RUDIMENTARY, DataComplexity.RUDIMENTARY, defaultVersion())

    it('creates', () => {
        range(100).forEach(i => expect(gen.create(i)).toMatch(/^[ADE]$/))
    })

    it('does not simplify', () => {
        range(100).forEach(i => expect([...gen.simplify(i)].length).toBe(0))
    })
})

describe('map', () => {
    const gen = map(ints({min: 1, max: 10}), x => x * 2)

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val).toBeGreaterThanOrEqual(2)
            expect(val).toBeLessThanOrEqual(20)
            expect(val % 2).toEqual(0)
        })
    })

    it('does not simplify', () => {
        range(100).forEach(i => expect([...gen.simplify(i)].length).toBe(0))
    })
})

describe('filter', () => {
    const gen = filter(ints({min: 1, max: 10}), x => x % 2 == 0)

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val).toBeGreaterThanOrEqual(2)
            expect(val).toBeLessThanOrEqual(10)
            expect(val % 2).toEqual(0)
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
    })
})

describe('oneOf', () => {
    const gen = oneOf(
        ints({min: 1, max: 10}),
        ints({min: 100, max: 110})
    )

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            if (val < 50) {
                expect(val).toBeGreaterThanOrEqual(1)
                expect(val).toBeLessThanOrEqual(10)
            }
            else {
                expect(val).toBeGreaterThanOrEqual(100)
                expect(val).toBeLessThanOrEqual(110)
            }
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
    })
})

describe('array', () => {
    const gen = array(ints({min: 1, max: 10}), {maxLen: 10})

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val.length).toBeLessThanOrEqual(10)
            expect(val.length).toBeGreaterThanOrEqual(0)
            for (const v of val) {
                expect(v).toBeGreaterThanOrEqual(1)
                expect(v).toBeLessThanOrEqual(10)
            }
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
    })
})

describe('tuple', () => {
    const gen = tuple(ints({min: 1, max: 100}), ints({min: 1, max: 100}), ints({min: 1, max: 100}))

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val.length).toBe(3)
            for (const v of val) {
                expect(v).toBeGreaterThanOrEqual(1)
                expect(v).toBeLessThanOrEqual(100)
            }
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
        const vals = [...gen.simplify(7)]

        // We should be starting with different values in all three spots
        expect(vals[0]).not.toEqual((new Array(3)).fill(vals[0][0]))

        // We should still have different values after a few iterations
        expect(vals.slice(10)[0]).not.toEqual((new Array(3)).fill(vals.slice(10)[0][0]))

        // We should end with matching simplified values
        expect(vals.slice(-1)[0]).toEqual((new Array(3)).fill(vals.slice(-1)[0][0]))
    })
})
