import {index, defaultVersion, map, ValGen} from "../common";
import colorData from './data/colors'
import {random, randomFloat} from "../../core";

/**
 * Interface for named colors
 */
export interface Named {
    /**
     * CSS (or extended CSS) name of the color
     * Example: 'black'
     */
    name: string

    /**
     * Hex value of the color (no prefix)
     * Example: 0FC8D9
     */
    hex: string

    /**
     * Red value of the color (0-255)
     * Example: 128
     */
    red: number

    /**
     * Green value of the color (0-255)
     * Example: 128
     */
    green: number

    /**
     * Blue value of the color (0-255)
     * Example: 128
     */
    blue: number
}

/**
 * Interface for RGB colors (red, green, blue)
 */
export interface RGB {
    /**
     * Red value of the color (0-255)
     * Example: 128
     */
    red: number

    /**
     * Green value of the color (0-255)
     * Example: 128
     */
    green: number

    /**
     * Blue value of the color (0-255)
     * Example: 128
     */
    blue: number

    /**
     * CSS string of the color
     * Example: rgb(120, 10, 44)
     */
    css : string

    /**
     * Hex string of the color (no prefix)
     * Example: 0FC8D9
     */
    hex : string
}


/**
 * Interface for RGBA colors (red, green, blue, alpha)
 */
export interface RGBA extends RGB {
    /**
     * Alpha value of the color
     * Example: 127
     */
    alpha: number

    /**
     * Hex string of the color (no prefix)
     * Example: 0FC8D97F
     */
    hex : string
}


/**
 * Interface for HSL colors (hue, saturation, luminosity)
 */
export interface HSL {
    /**
     * Hue of the color (degrees, between 0-360)
     * Example: 249.49
     */
    hue: number

    /**
     * Saturation of the color (0-1)
     * Example: 0.76
     */
    saturation: number

    /**
     * Luminosity of the color (0-1)
     * Example: 0.26
     */
    luminosity: number

    /**
     * CSS string for the color
     * Example: hsl(250.23, 20%, 10%)
     */
    css : string
}

export interface HSLA extends HSL {
    /**
     * Alpha value for the color (between 0-1)
     * Example: 0.34
     */
    alpha: number

    /**
     * CSS string for the color
     * Example: hsla(250.23, 20%, 10%, 0.4)
     */
    css : string
}

function hexify(byte: number) {
    const r = byte.toString(16)
    if (r.length < 2) {
        return '0' + r
    }
    return r
}

/**
 * Generator for RGB colors
 */
export const rgb : ValGen<RGB> = {
    create: function (seed: number) {
        const rnd = random(seed)
        const red = rnd() % 255
        const green = rnd() % 255
        const blue = rnd() % 255
        const css = `rgb(${red}, ${green}, ${blue})`
        const hex = `${hexify(red)}${hexify(green)}${hexify(blue)}`
        return {
            red, green, blue, css, hex
        }
    },
    simplify: function* (_seed: number) {}
}

/**
 * Generator for RGBA colors
 */
export const rgba : ValGen<RGBA> = {
    create: function (seed) {
        const rnd = random(seed)
        const red = rnd() % 255
        const green = rnd() % 255
        const blue = rnd() % 255
        const alpha = rnd() % 255
        const css = `rgba(${red}, ${green}, ${blue}, ${alpha})`
        const hex = `${hexify(red)}${hexify(green)}${hexify(blue)}${hexify(alpha)}`
        return {
            red, green, blue, alpha, css, hex
        }
    },
    simplify: function* (_seed: number) {}
}

/**
 * Generator for HSL colors
 */
export const hsl : ValGen<HSL> = {
    create(seed: number): HSL {
        const rnd = randomFloat(seed)
        const hue = rnd() * 360
        const saturation = rnd()
        const luminosity = rnd()
        const css = `hsl(${hue}, ${saturation * 100}%, ${luminosity * 100}%)`
        return {hue, saturation, luminosity, css}
    },
    simplify: function* (_seed: number) {}
}

/**
 * Generator for HSLA colors
 */
export const hsla : ValGen<HSLA> = {
    create(seed: number): HSLA {
        const rnd = randomFloat(seed)
        const hue = rnd() * 360
        const saturation = rnd()
        const luminosity = rnd()
        const alpha = rnd()
        const css = `hsla(${hue}, ${saturation * 100}%, ${luminosity * 100}%, ${alpha})`
        return {hue, saturation, luminosity, alpha, css}
    },
    simplify: function* (_seed: number) {}
}

/**
 * Generator for named colors
 */
export function named(dataVersion = defaultVersion()) {
    return map(index(colorData, dataVersion), function (val: any) : Named {
        return {
            name: val.name,
            hex: val.hex,
            red: +val.red,
            green: +val.green,
            blue: +val.blue
        }
    })
}