import {hsl, hsla, named, rgb, rgba} from "./index";
import {range} from "../../core";

describe('colors', () => {
    it('rgb', () => {
        range(100).forEach(
            i => expect(rgb.create(i).hex)
                .toMatch(/^[a-fA-F\d]{6}$/)
        )
    })

    it('rgba', () => {
        range(100).forEach(
            i => expect(rgba.create(i).hex)
                .toMatch(/^[a-fA-F\d]{8}$/)
        )
    })

    it('hsl', () => {
        range(100).forEach(
            i => expect(hsl.create(i).css)
                .toMatch(/^hsl\(\d+(\.\d+)?\s*,\s*\d+(\.\d+)?%\s*,\s*\d+(\.\d+)?%\)$/)
        )
    })

    it('hsla', () => {
        range(100).forEach(
            i => expect(hsla.create(i).css)
                .toMatch(/^hsla\(\d+(\.\d+)?\s*,\s*\d+(\.\d+)?\s*%,\s*\d+(\.\d+)?\s*%,\s*\d+(\.\d+)?\)$/)
        )
    })

    it('named', () => {
        range(100).forEach(
            i => expect(named().create(i).hex)
                .toMatch(/^[a-fA-F\d]{6}$/)
        )
    })
})