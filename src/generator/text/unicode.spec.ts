import {Category, fromCategories} from "./unicode";
import {range} from "../../core";

describe('unicode', () => {
    it('fromCategories', () => {
        range(100)
            .forEach(
                i => expect(
                    fromCategories(
                        [Category.Ll, Category.Lu]
                    ).create(i)
                ).toBeTruthy()
            )
    })
})