import * as ascii from './ascii'
import {range} from "../../core";

describe('ASCII', () => {
    const numIters = 500

    it('any', () => {
        const gen = ascii.any()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x00-\x7f]+$/))
        const simplified = [...gen.simplify(100)]
        expect(simplified.length).toBeGreaterThan(10)
        expect(simplified.slice(-1)[0].length).toBeLessThan(gen.create(100).length)
    })

    it('whitespace', () => {
        const gen = ascii.whitespace()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^\s+$/))
    })

    it('control', () => {
        const gen = ascii.control()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x00-\x1F\x7F]+$/))
    })

    it('alpha', () => {
        const gen = ascii.alpha()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-zA-Z]+$/))
    })

    it('alphaLower', () => {
        const gen = ascii.alphaLower()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-z]+$/))
    })

    it('alphaUpper', () => {
        const gen = ascii.alphaUpper()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[A-Z]+$/))
    })

    it('numeric', () => {
        const gen = ascii.numeric()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^\d+$/))
    })

    it('alphaNumeric', () => {
        const gen = ascii.alphaNumeric()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-zA-Z\d]+$/))
    })

    it('alphaNumericLower', () => {
        const gen = ascii.alphaNumericLower()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-z\d]+$/))
    })

    it('alphaNumericUpper', () => {
        const gen = ascii.alphaNumericUpper()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[A-Z\d]+$/))
    })

    it('octal', () => {
        const gen = ascii.octal()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[0-7]+$/))
    })

    it('binary', () => {
        const gen = ascii.binary()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[0-1]+$/))
    })

    it('domain piece', () => {
        const gen = ascii.domainPiece()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x41-\x5A\x61-\x7A\x30-\x39\x2D]+$/))
    })

    it('symbol', () => {
        const gen = ascii.symbol()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x21-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E]+$/))
    })

    it('html named', () => {
        const gen = ascii.htmlNamed()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x21-\x2C\x2E-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E]+$/))
    })

    it('quote', () => {
        const gen = ascii.quote()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x27\x22\x60]+$/))
    })

    it('bracket', () => {
        const gen = ascii.bracket()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x28-\x29\x3C\x3E\x7B\x7D\x5B\x5D]+$/))
    })

    it('sentence punctuation', () => {
        const gen = ascii.sentencePunctuation()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x2C-\x2E\x22\x3F\x3A-\x3B\x27-\x29]+$/))
    })

    it('safe punctuation', () => {
        const gen = ascii.safePunctuation()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x2C\x2E]+$/))
    })

    it('printable', () => {
        const gen = ascii.printable()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x09\x0A-\x0D\x20-\x7E]+$/))
    })

    it('text', () => {
        const gen = ascii.text()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x09\x0A-\x0D\x20\x2C-\x2E\x22\x3F\x3A-\x3B\x27-\x29\x41-\x5A\x61-\x7A\x30-\x39]+$/))
    })

    it('math', () => {
        const gen = ascii.math()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x09\x0A-\x0D\x20-\x21\x25\x28-\x3E\x5E\x41-\x5A\x61-\x7A]+$/))
    })

    it('hex', () => {
        const gen = ascii.hex()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-fA-F0-9]+$/))
    })

    it('hexUpper', () => {
        const gen = ascii.hexUpper()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[A-F0-9]+$/))
    })

    it('hexLower', () => {
        const gen = ascii.hexLower()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-f0-9]+$/))
    })
})
