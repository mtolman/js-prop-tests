import {ValGen} from "../common";
import {charset, CharSet, defaultOptions, Options} from "./common";
import data from './data/ascii_char_groups'

function get_charsets(options: Options, ...charsets: string[]) : CharSet {
    const dataset = data[options.dataVersion || defaultOptions.dataVersion]
    return {
        ranges: charsets.flatMap(charset => {
            return dataset[charset].charactersHexRange
        })
    }
}

/**
 * Returns a string with any ASCII character
 * @param options Generation options
 */
export function any(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'ANY'), options)
}

/**
 * Returns a string with whitespace ASCII characters
 * @param options Generation options
 */
export function whitespace(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'WHITESPACE'), options)
}

/**
 * Returns a string with control ASCII characters
 * @param options Generation options
 */
export function control(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'CONTROL'), options)
}

/**
 * Returns a string with uppercase alphabetic ASCII characters
 * @param options Generation options
 */
export function alphaUpper(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'ALPHA_UPPER'), options)
}

/**
 * Returns a string with lowercase alphabetic ASCII characters
 * @param options Generation options
 */
export function alphaLower(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'ALPHA_LOWER'), options)
}

/**
 * Returns a string with alphabetic ASCII characters
 * @param options Generation options
 */
export function alpha(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'ALPHA'), options)
}

/**
 * Returns a string with numeric ASCII characters
 * @param options Generation options
 */
export function numeric(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'NUMERIC'), options)
}

/**
 * Returns a string with numeric and uppercase alphabetic ASCII characters
 * @param options Generation options
 */
export function alphaNumericUpper(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'ALPHA_NUMERIC_UPPER'), options)
}

/**
 * Returns a string with numeric and lowercase alphabetic ASCII characters
 * @param options Generation options
 */
export function alphaNumericLower(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'ALPHA_NUMERIC_LOWER'), options)
}

/**
 * Returns a string with numeric and alphabetic ASCII characters
 * @param options Generation options
 */
export function alphaNumeric(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'ALPHA_NUMERIC'), options)
}

/**
 * Returns a string with octal ASCII characters
 * @param options Generation options
 */
export function octal(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'OCTAL'), options)
}

/**
 * Returns a string with binary ASCII characters
 * @param options Generation options
 */
export function binary(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'BINARY'), options)
}

/**
 * Returns a string with valid domain piece characters
 * @param options Generation options
 */
export function domainPiece(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'DOMAIN_PIECE'), options)
}

/**
 * Returns a string with symbol ASCII characters
 * @param options Generation options
 */
export function symbol(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'SYMBOL'), options)
}

/**
 * Returns a string with ASCII characters that have named HTML escape codes
 * @param options Generation options
 */
export function htmlNamed(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'HTML_NAMED'), options)
}

/**
 * Returns a string with quotation mark ASCII characters (marks interpreted as quote marks in several programming languages)
 * @param options Generation options
 */
export function quote(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'QUOTE'), options)
}

/**
 * Returns a string with brackets and parenthesis (e.g. []{}()<>) ASCII characters
 * @param options Generation options
 */
export function bracket(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'BRACKET'), options)
}

/**
 * Returns a string with sentence punctuation ASCII characters
 * @param options Generation options
 */
export function sentencePunctuation(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'SENTENCE_PUNCTUATION'), options)
}

/**
 * Returns a string with punctuation ASCII characters that generally don't cause issues in markup documents (HTML, Markdown, etc)
 * @param options Generation options
 */
export function safePunctuation(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'SAFE_PUNCTUATION'), options)
}

/**
 * Returns a string with uppercase hex ASCII characters
 * @param options Generation options
 */
export function hexUpper(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'HEX_UPPER'), options)
}

/**
 * Returns a string with lowercase hex ASCII characters
 * @param options Generation options
 */
export function hexLower(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'HEX_LOWER'), options)
}

/**
 * Returns a string with hex ASCII characters
 * @param options Generation options
 */
export function hex(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'HEX'), options)
}

/**
 * Returns a string with printable ASCII characters
 * @param options Generation options
 */
export function printable(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'PRINTABLE'), options)
}

/**
 * Returns a string with common English text ASCII characters
 * @param options Generation options
 */
export function text(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'TEXT'), options)
}

/**
 * Returns a string with mathematics ASCII characters
 * **Note:** This does NOT create valid math equations!
 * @param options Generation options
 */
export function math(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'MATH'), options)
}

/**
 * Returns a string with ASCII characters that don't need to be URL encoded
 * @param options Generation options
 */
export function urlUnencodedChars(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'URL_UNENCODED_CHARS'), options)
}

/**
 * Returns a string with ASCII characters that don't need to be URL encoded inside a hash segment
 * @param options Generation options
 */
export function urlUnencodedHashChars(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'URL_UNENCODED_HASH_CHARS'), options)
}

/**
 * Returns an empty string with ASCII string
 * @param options Generation options
 */
export function empty(options: Options = {}): ValGen<string> {
    return charset(get_charsets(options, 'EMPTY'), options)
}