import {ValGen, defaultVersion} from "../common";
import {random} from "../../core";

/**
 * String generation options
 */
export interface Options {
    /**
     * Minimum length
     */
    minLen?: number
    /**
     * Maximum length
     */
    maxLen?: number
    /**
     * Data set version to use (defaults to latest)
     */
    dataVersion?: string
}

/**
 * @ignore
 */
export const defaultOptions = {
    minLen: 1,
    maxLen: 1024,
    dataVersion: defaultVersion() as string
}

/**
 * Represents a range of characters
 */
export interface CharRange {
    start: number
    end: number
}


/**
 * Represents a character set, which is a collection of character ranges
 */
export interface CharSet {
    ranges: CharRange[]
}

/**
 * Creates a string from a character set
 * @param characterSet Character set to use
 * @param seed Seed to use
 * @param maxLen Maximum string length
 * @param minLen Minimum string length
 */
function createString(characterSet: CharSet, seed: number, maxLen: number, minLen: number): string {
    if (characterSet.ranges.length === 0) {
        return ''
    }
    const rnd = random(seed)
    const len = rnd() % (maxLen - minLen + 1) + minLen
    let res = ''
    for (let i = 0; i < len; ++i) {
        const charSet = characterSet.ranges[rnd() % characterSet.ranges.length]
        const codePoint = rnd() % (charSet.end - charSet.start + 1) + charSet.start
        res += String.fromCodePoint(codePoint)
    }
    return res
}

/**
 * Creates strings from a character set
 * @param characterSet Character set to use
 * @param options String creation options
 */
export function charset(characterSet: CharSet, options: Options = {}): ValGen<string> {
    const fixedOptions = {...defaultOptions, ...options}
    const {maxLen, minLen} = fixedOptions
    return {
        create: seed => createString(characterSet, seed, maxLen, minLen),
        simplify: function*(seed: number) {
            const str = createString(characterSet, seed, maxLen, minLen)
            let len = str.length
            while ((--len) >= minLen) {
                yield createString(characterSet, seed, len, len)
            }
        }
    }
}