import * as eascii from './extended_ascii'

const range = (i: number): number[] => [...(new Array(i)).keys()]

describe('EASCII', () => {
    const numIters = 500

    it('any', () => {
        const gen = eascii.any()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x00-\xff]+$/))
    })

    it('extended', () => {
        const gen = eascii.extended()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x80-\xff]+$/))
    })

    it('whitespace', () => {
        const gen = eascii.whitespace()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^\s+$/))
    })

    it('control', () => {
        const gen = eascii.control()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x00-\x1F\x7F]+$/))
    })

    it('alpha', () => {
        const gen = eascii.alpha()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-zA-Z]+$/))
    })

    it('alphaLower', () => {
        const gen = eascii.alphaLower()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-z]+$/))
    })

    it('alphaUpper', () => {
        const gen = eascii.alphaUpper()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[A-Z]+$/))
    })

    it('numeric', () => {
        const gen = eascii.numeric()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^\d+$/))
    })

    it('alphaNumeric', () => {
        const gen = eascii.alphaNumeric()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-zA-Z\d]+$/))
    })

    it('alphaNumericLower', () => {
        const gen = eascii.alphaNumericLower()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-z\d]+$/))
    })

    it('alphaNumericUpper', () => {
        const gen = eascii.alphaNumericUpper()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[A-Z\d]+$/))
    })

    it('octal', () => {
        const gen = eascii.octal()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[0-7]+$/))
    })

    it('binary', () => {
        const gen = eascii.binary()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[0-1]+$/))
    })

    it('domain piece', () => {
        const gen = eascii.domainPiece()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x41-\x5A\x61-\x7A\x30-\x39\x2D]+$/))
    })

    it('symbol', () => {
        const gen = eascii.symbol()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x21-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E]+$/))
    })

    it('html named', () => {
        const gen = eascii.htmlNamed()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x21-\x2C\x2E-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E]+$/))
    })

    it('quote', () => {
        const gen = eascii.quote()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x27\x22\x60]+$/))
    })

    it('bracket', () => {
        const gen = eascii.bracket()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x28-\x29\x3C\x3E\x7B\x7D\x5B\x5D]+$/))
    })

    it('sentence punctuation', () => {
        const gen = eascii.sentencePunctuation()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x2C-\x2E\x22\x3F\x3A-\x3B\x27-\x29]+$/))
    })

    it('safe punctuation', () => {
        const gen = eascii.safePunctuation()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x2C\x2E]+$/))
    })

    it('printable', () => {
        const gen = eascii.printable()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x09\x0A-\x0D\x20-\x7E]+$/))
    })

    it('text', () => {
        const gen = eascii.text()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x09\x0A-\x0D\x20\x2C-\x2E\x22\x3F\x3A-\x3B\x27-\x29\x41-\x5A\x61-\x7A\x30-\x39]+$/))
    })

    it('math', () => {
        const gen = eascii.math()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[\x09\x0A-\x0D\x20-\x21\x25\x28-\x3E\x5E\x41-\x5A\x61-\x7A]+$/))
    })

    it('hex', () => {
        const gen = eascii.hex()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-fA-F0-9]+$/))
    })

    it('hexUpper', () => {
        const gen = eascii.hexUpper()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[A-F0-9]+$/))
    })

    it('hexLower', () => {
        const gen = eascii.hexLower()
        range(numIters).map(r => gen.create(r)).forEach(v => expect(v).toMatch(/^[a-f0-9]+$/))
    })
})
