import {defaultVersion, ValGen} from "../common";
import {CharRange, CharSet} from "./common";
import unicodeCharCategories from './data/unicode_char_categories'
import {random, range} from "../../core";

/**
 * Unicode categories
 */
export enum Category {
    Cc,
    Cf,
    Co,
    Cs,
    Ll,
    Lm,
    Lo,
    Lt,
    Lu,
    Mc,
    Me,
    Mn,
    Nd,
    Nl,
    No,
    Pc,
    Pd,
    Pe,
    Pf,
    Pi,
    Po,
    Ps,
    Sc,
    Sk,
    Sm,
    So,
    Zl,
    Zp,
    Zs,
}

/**
 * Options for unicode categories
 */
export interface Options {
    minLen?: number
    maxLen?: number
}

const defaultOptions = {
    minLen: 1,
    maxLen: 1024,
}

function lookupCharSet(version: string, category: Category): CharSet {
    const key = categoryToKey(category)
    return {ranges: unicodeCharCategories[version][key].hexRanges}
}

export interface UnicodeGenerators {
    /**
     * Creates unicode strings where characters belong to specific categories
     * @param categories Categories characters should belong to
     * @param options String generation options
     */
    fromCategories(categories: Category[], options?: Options): ValGen<string>
}

/**
 * Locks unicode generators to a specific version
 * @param dataVersion Version to lock to
 */
export function withVersion(dataVersion: string) {
    function fromCategories(categories: Category[], options: Options = {}) : ValGen<string> {
        const opts = {...defaultOptions, options}
        const availCharSets: CharSet = {ranges: []};
        for (const category of categories) {
            availCharSets.ranges = availCharSets.ranges.concat(...(lookupCharSet(dataVersion, category).ranges))
        }

        const create = (seed: number): string => {
            const rnd = random(seed);
            let len: number;
            if (opts.maxLen <= opts.minLen) {
                const _ = rnd();
                len = opts.minLen;
            }
            else {
                len = (rnd() % (opts.maxLen - opts.minLen + 1)) + opts.minLen;
            }
            const res: number[] = range(len);

            for (let i = 0; i < res.length; ++i) {
                res[i] = charFromRanges(availCharSets, rnd());
            }

            return res.map(c => String.fromCodePoint(c) || '').join('');
        }
        return {
            create,
            simplify: function*(seed: number) {
                let len = create(seed).length
                while (--len > opts.minLen) {
                    yield fromCategories(categories, {minLen: len, maxLen: len}).create(seed)
                }
            }
        }
    }
    return {
        fromCategories
    };
}

/**
 * Converts a category enum to a string key
 * @param category Category to convert
 */
export function categoryToKey(category: Category) {
    switch (category) {
        case Category.Cc:
            return 'Cc'
        case Category.Cf:
            return 'Cf'
        case Category.Co:
            return 'Co'
        case Category.Cs:
            return 'Cs'
        case Category.Ll:
            return 'Ll'
        case Category.Lm:
            return 'Lm'
        case Category.Lo:
            return 'Lo'
        case Category.Lt:
            return 'Lt'
        case Category.Lu:
            return 'Lu'
        case Category.Mc:
            return 'Mc'
        case Category.Me:
            return 'Me'
        case Category.Mn:
            return 'Mn'
        case Category.Nd:
            return 'Nd'
        case Category.Nl:
            return 'Nl'
        case Category.No:
            return 'No'
        case Category.Pc:
            return 'Pc'
        case Category.Pd:
            return 'Pd'
        case Category.Pe:
            return 'Pe'
        case Category.Pf:
            return 'Pf'
        case Category.Pi:
            return 'Pi'
        case Category.Po:
            return 'Po'
        case Category.Ps:
            return 'Ps'
        case Category.Sc:
            return 'Sc'
        case Category.Sk:
            return 'Sk'
        case Category.Sm:
            return 'Sm'
        case Category.So:
            return 'So'
        case Category.Zl:
            return 'Zl'
        case Category.Zp:
            return 'Zp'
        case Category.Zs:
            return 'Zs'
    }
}

function rangeSize(range: CharRange) {
    return range.end - range.start + 1
}

function charFromRanges(charSet: CharSet, index: number): number {
    index = index >>> 0
    if (charSet.ranges.length == 0) {
        return 0;
    }

    let rangeIndex = 0;
    let curRange = charSet.ranges[rangeIndex];
    let totalSize = 0;
    while (index >= rangeSize(curRange)) {
        index -= rangeSize(curRange);
        totalSize += rangeSize(curRange);

        if (rangeIndex + 1 < charSet.ranges.length) {
            curRange = charSet.ranges[++rangeIndex];
        } else {
            return charFromRanges(charSet, index % totalSize);
        }
    }

    return curRange.start + index;
}

const res = withVersion(defaultVersion())

/**
 * Creates unicode strings where characters belong to specific categories
 * @param categories Categories characters should belong to
 * @param options String generation options
 */
export const fromCategories = res.fromCategories
