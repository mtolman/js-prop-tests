import {
    cmakeSystemNames,
    cpuArchitectures,
    fileExtensions,
    fileMimeMapping,
    fileTypes,
    mimeTypes,
    operatingSystems
} from "./index";
import {range} from "../../core";

describe('computers', () => {
    it('cmakeSystemNames', () => {
        range(10).forEach(i => expect(cmakeSystemNames.create(i)).toMatch(/^\w+$/))
    })

    it('cpuArchitectures', () => {
        range(100).forEach(i => expect(cpuArchitectures.create(i)).toMatch(/^\w+$/))
    })

    it('fileExtensions', () => {
        range(100).forEach(i => expect(fileExtensions.create(i)).toMatch(/^[^.]+$/))
    })

    it('fileTypes', () => {
        range(100).forEach(i => expect(fileTypes.create(i)).toMatch(/^\w+$/))
    })

    it('fileMimeMapping', () => {
        range(100).forEach(i => expect(fileMimeMapping.create(i).mime).toMatch(/^[\w\-+.]+\/[\w\-+.]+$/))
        range(100).forEach(i => expect(fileMimeMapping.create(i).extension).toMatch(/^[^.]+$/))
    })

    it('mimeTypes', () => {
        range(100).forEach(i => expect(mimeTypes.create(i)).toMatch(/^[\w\-+.]+\/[\w\-+.]+$/))
    })

    it('operatingSystems', () => {
        range(100).forEach(i => expect(operatingSystems.create(i)).toMatch(/^\w+$/))
    })
})