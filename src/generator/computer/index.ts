import {defaultVersion, index, ValGen} from "../common";
import cmakeSystemNamesData from './data/cmake_system_names'
import cpuArchitecturesData from './data/cpu_architectures'
import fileExtensionsData from './data/file_extensions'
import fileMimeMappingData from './data/file_mime_mapping'
import fileTypesData from './data/file_types'
import mimeTypesData from './data/mime_types'
import osData from './data/os'

/**
 * Interface for common Mime to file extension mappings
 */
export interface MimeMapping {
    /**
     * Mime type
     * Example: application/json
     */
    mime: string,

    /**
     * file extension (no periods)
     * Example: json
     */
    extension: string
}

/**
 * Interface for computer generators that are subject to data versioning
 */
export interface ComputerGenerators {
    /**
     * Generator for CMake system names
     * Example: Windows, Darwin, Linux
     */
    cmakeSystemNames: ValGen<string>

    /**
     * Generator for CPU architectures
     */
    cpuArchitectures: ValGen<string>

    /**
     * Generator for common file extensions
     */
    fileExtensions: ValGen<string>

    /**
     * Generator for common file to mime type mappings
     */
    fileMimeMapping: ValGen<MimeMapping>

    /**
     * Generator for common file types
     */
    fileTypes: ValGen<string>

    /**
     * Generator for common mime types
     */
    mimeTypes: ValGen<string>

    /**
     * Generator for different operating system names
     */
    operatingSystems: ValGen<string>
}

/**
 * Retrieves versioned computer generators using a specific data version
 * @param dataVersion Data version to use
 */
export function withVersion(dataVersion: string): ComputerGenerators {
    return {
        cmakeSystemNames: index(cmakeSystemNamesData, dataVersion),
        cpuArchitectures: index(cpuArchitecturesData, dataVersion),
        fileExtensions: index(fileExtensionsData, dataVersion),
        fileMimeMapping: index(fileMimeMappingData, dataVersion),
        fileTypes: index(fileTypesData, dataVersion),
        mimeTypes: index(mimeTypesData, dataVersion),
        operatingSystems: index(osData, dataVersion),
    }
}

const res = withVersion(defaultVersion())

/**
 * Generator for CMake system names
 * Uses latest data version
 */
export const cmakeSystemNames = res.cmakeSystemNames

/**
 * Generator for CPU architectures
 * Uses latest data version
 */
export const cpuArchitectures = res.cpuArchitectures

/**
 * Generator for common file extensions
 * Uses latest data version
 */
export const fileExtensions = res.fileExtensions

/**
 * Generator for common file to mime type mappings
 * Uses latest data version
 */
export const fileMimeMapping = res.fileMimeMapping

/**
 * Generator for common file types
 * Uses latest data version
 */
export const fileTypes = res.fileTypes

/**
 * Generator for common mime types
 * Uses latest data version
 */
export const mimeTypes = res.mimeTypes

/**
 * Generator for different operating system names
 * Uses latest data version
 */
export const operatingSystems = res.operatingSystems
