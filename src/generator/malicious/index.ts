import {defaultVersion, index, ValGen, map} from "../common";
import edgeCaseData from './data/edge_cases'
import formatInjectionData from './data/format_injection'
import sqlInjectionData from './data/sql_injection'
import xssData from './data/xss'

/**
 * Interface for generators which are dependent on data sets
 */
export interface MaliciousGenerators {
    /**
     * Generator for edge cases designed to break code or cause issues with inter-process communication
     */
    edgeCase: ValGen<string>

    /**
     * Generator for format injection attacks (attacks designed to abuse string formatting vulnerabilities)
     * (e.g. C's printf, Java Log4Shell, Python Format, etc.)
     */
    formatInjection: ValGen<string>

    /**
     * Generator for SQL injection attacks
     */
    sqlInjection: ValGen<string>

    /**
     * Generator for XSS attacks
     */
    xss: ValGen<string>
}

/**
 * Creates version locked generators
 * @param dataVersion Version to lock to
 */
export function withVersion(dataVersion: string): MaliciousGenerators {
    return {
        edgeCase: map(
            index(edgeCaseData, dataVersion),
            (edgeCase: {name: string;bytes: {buffer: Buffer}}) => {
                return edgeCase.bytes.buffer.toString()
            }
        ),
        formatInjection: index(formatInjectionData, dataVersion),
        sqlInjection: index(sqlInjectionData, dataVersion),
        xss: index(xssData, dataVersion),
    }
}

const res = withVersion(defaultVersion())

/**
 * Generator for edge cases designed to break code or cause issues with inter-process communication
 */
export const edgeCase = res.edgeCase

/**
 * Generator for format injection attacks (attacks designed to abuse string formatting vulnerabilities)
 * (e.g. C's printf, Java Log4Shell, Python Format, etc.)
 */
export const formatInjection = res.formatInjection

/**
 * Generator for SQL injection attacks
 */
export const sqlInjection = res.sqlInjection

/**
 * Generator for XSS attacks
 */
export const xss = res.xss
