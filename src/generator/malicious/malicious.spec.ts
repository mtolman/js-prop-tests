import {edgeCase, formatInjection, sqlInjection, xss} from "./index";
import {range} from "../../core";

describe('malicious', () => {
    it('edge cases', () => {
        range(100).forEach(i => edgeCase.create(i))
    })

    it('format injection', () => {
        range(100).forEach(i => expect(formatInjection.create(i)).toBeTruthy())
    })

    it('sql injection', () => {
        range(100).forEach(i => expect(sqlInjection.create(i)).toBeTruthy())
    })

    it('xss', () => {
        range(100).forEach(i => expect(xss.create(i)).toBeTruthy())
    })
})