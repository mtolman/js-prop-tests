import {
    index,
    ValGen,
    map,
    complexity,
    DataComplexity, filter, defaultVersion
} from "../common";
import {alphaUpper, numeric} from "../text/ascii";
import {floats} from "../number";
import {withVersion as worldWithVersion} from "../world";
import ccNetworks from './data/credit_card_networks'
import currencyData from './data/currencies'
import transactionTypeData from './data/transaction_types'
import accountNameData from './data/account_names'
import {Random, random} from "../../core";

/**
 * Interface for credit card data
 */
export interface CreditCard {
    /**
     * Card CVV number
     */
    cvv: string,

    /**
     * Card network id
     */
    networkId: string,

    /**
     * Card network name
     */
    networkName: string,

    /**
     * Card number
     */
    number: string,

    /**
     * Card expiration date (MM/YYYY)
     */
    expiration: string
}

/**
 * Information about a currency recognized by ISO
 */
export interface Currency {
    /**
     * ISO code for the currency
     */
    isoCode: string

    /**
     * Common name for the currency
     */
    name: string

    /**
     * Symbol for the currency (or an empty string if there is none)
     */
    symbol: string
}

/**
 * Position to put the symbol when creating an amount
 */
export enum SymbolPos {
    PREFIX,
    SUFFIX
}

/**
 * Options for generating amounts
 */
export interface AmountOptions {
    /**
     * Maximum value. Default: 10000000000
     */
    max?: number

    /**
     * Minimum value. Default: 0
     */
    min?: number

    /**
     * Decimal places to include. Default: 2
     */
    decimalPlaces?: number

    /**
     * Decimal separator to use. Default: "."
     */
    decimalSeparator?: string

    /**
     * Currency symbol to use. Default is none
     */
    symbol?: string

    /**
     * Position of the currency symbol
     */
    symbolPos?: SymbolPos
}

const defaultAmountOptions = {
    max: 10000000000,
    min: 0,
    decimalPlaces: 2,
    decimalSeparator: ".",
    symbol: '',
    symbolPos: SymbolPos.PREFIX
}

/**
 * Options for generating BIC codes
 */
export interface BicOptions {
    /**
     * Whether to include a branch code. Default: false
     */
    includeBranchCode?: boolean
}

const defaultBicOptions = {
    includeBranchCode: false
}

function bicMaker(dataVersion: string = defaultVersion()) {
    return (options: BicOptions = {}) : ValGen<string> => {
        const opts: {includeBranchCode: boolean} = {...defaultBicOptions, ...options}
        return {
            create(seed: number): string {
                const rand = random(seed)
                let str = ''

                // institution/bank code
                str += alphaUpper({minLen: 4, maxLen: 4}).create(rand())

                // country code
                str += filter(worldWithVersion(dataVersion).country, d => !!d.alpha2).create(rand()).alpha2

                // location code
                str += alphaUpper({minLen: 2, maxLen: 2}).create(rand())

                if (opts.includeBranchCode) {
                    str += alphaUpper({minLen: 3, maxLen: 3}).create(rand())
                }

                return str;
            },
            simplify: function* (_seed: number) {}
        }
    }
}

function cardCheckDigit(number: string) : string {
    let sum = 0;
    let multiplier = 2;
    for (let i = number.length; i > 0; --i) {
        const index = i - 1;
        sum += (number.charCodeAt(index) - '0'.charCodeAt(0)) * multiplier;
        // flip between 1 and 2
        multiplier ^= 3;
    }
    return String.fromCharCode(9 - (sum + 9) % 10 + '0'.charCodeAt(0));
}

function cardNumImpl(creditCardNetwork: any, rnd: Random) : string {
    const lens = creditCardNetwork.lengths as string[]
    const starts = creditCardNetwork.starts as string[]
    const len = +lens[rnd() % lens.length]
    const start = starts[rnd() % starts.length] as string
    const genLen = len - 1 - start.length;
    const res = start + numeric({minLen: genLen, maxLen: genLen}).create(rnd());
    return res + cardCheckDigit(res);
}

function cardExpiration(rnd: Random) : string {
    let month = `${(rnd() % 12) + 1}`
    const year = (rnd() % 400) + 2000
    if (+month < 10) {
        month = '0' + month
    }
    return month + '/' + year
}

function creditCardMaker(version: string = defaultVersion()) : ValGen<CreditCard> {
    return {
        create(seed: number): CreditCard {
            const rnd = random(seed)
            const creditCardNetwork = ccNetworks[version][rnd() % ccNetworks[version].length]
            return {
                cvv: numeric({minLen: +creditCardNetwork.cvvlen, maxLen: +creditCardNetwork.cvvlen}).create(rnd()),
                networkId: creditCardNetwork.id,
                networkName: creditCardNetwork.name,
                number: cardNumImpl(creditCardNetwork, rnd),
                expiration: cardExpiration(rnd),
            }
        },
        simplify: function*(_seed: number){}
    }
}

/**
 * Options for creating masked numbers
 */
export interface MaskedNumberOptions {
    /**
     * Length of the unmasked portion. Default: 4
     */
    unmaskedLength?: number

    /**
     * Length of the masked portion. Default: 4
     */
    maskedLength?: number

    /**
     * Character to use for masking. Default: "*"
     */
    maskedChar?: string

    /**
     * Whether to surround with parenthesis. Default: false
     */
    surroundWithParens?: boolean
}

const defaultMaskedNumberOptions = {
    unmaskedLength: 4,
    maskedLength: 4,
    maskedChar: '*',
    surroundWithParens: false
}

/**
 * Interface for financial generators that can be versioned
 */
export interface FinancialGenerators {
    /**
     * Generator for account names
     * @param minComplexity Minimum data complexity. Defaults to DataComplexity.RUDIMENTARY
     * @param maxComplexity Maximum data complexity. Defaults to DataComplexity.COMPLEX
     */
    accountName(minComplexity?: DataComplexity, maxComplexity?: DataComplexity): ValGen<string>

    /**
     * Generator for transaction types. Meant to be used as an index for computers
     */
    transactionType: ValGen<string>

    /**
     * Generator for BICs
     * @param options BIC options
     */
    bic(options?: BicOptions): ValGen<string>

    /**
     * Generator for credit card information
     * Uses Luhn Check for the card number
     */
    creditCard: ValGen<CreditCard>

    /**
     * Generator for currencies
     */
    currency: ValGen<Currency>
}

/**
 * Creates versionable financial generators tied to a specific dataset version
 * @param dataVersion Data set version to tie to
 */
export function withVersion(dataVersion: string): FinancialGenerators {
    return {
        bic: bicMaker(dataVersion),
        creditCard: creditCardMaker(dataVersion),
        currency: index(currencyData, dataVersion),
        transactionType: index(transactionTypeData, dataVersion),
        accountName: (minComplexity = DataComplexity.BASIC, maxComplexity = DataComplexity.COMPLEX) => {
            return map(
                complexity(accountNameData, minComplexity, maxComplexity, dataVersion),
                val => {
                    return val.name
                }
            )
        }
    }
}


const res = withVersion(defaultVersion())

/**
 * Generator for BICs
 * @param options BIC options
 */
export const bic = res.bic

/**
 * Generator for credit card information
 * Uses Luhn Check for the card number
 */
export const creditCard = res.creditCard

/**
 * Generator for currencies
 */
export const currency = res.currency

/**
 * Generator for transaction types. Meant to be used as an index for computers
 */
export const transactionType = res.transactionType

/**
 * Generator for account names
 * @param minComplexity Minimum data complexity. Defaults to DataComplexity.RUDIMENTARY
 * @param maxComplexity Maximum data complexity. Defaults to DataComplexity.COMPLEX
 */
export const accountName = res.accountName

/**
 * Generator for a monetary amount
 * @param options Amount options
 */
export function amount(options: AmountOptions = {}): ValGen<string> {
    const {
        max,
        min,
        decimalPlaces,
        decimalSeparator,
        symbol,
        symbolPos
    } = {...defaultAmountOptions, ...options}

    const to_str = (float: number): string => {
        const str = float.toFixed(decimalPlaces).replace('.', decimalSeparator)
        if (symbol) {
            return symbolPos === SymbolPos.PREFIX ? symbol + str : str + symbol
        }
        return str
    }

    const gen = floats({min: min, max: max})

    const create = (seed: number): string => {
        return to_str(gen.create(seed))
    }
    return {
        create,
        simplify: function*(seed: number) {
            for (const v of gen.simplify(seed)) {
                yield to_str(v)
            }
        }
    }
}

/**
 * Generator for a pin
 * @param length Length of the pin. Default: 4
 */
export function pin(length: number = 4) {
    return numeric({maxLen: length, minLen: length})
}

/**
 * Generator for a masked number
 * @param options Options for the masked number
 */
export function maskedNumber(options: MaskedNumberOptions = {}): ValGen<string> {
    return {
        create(seed: number): string {
            const {unmaskedLength, maskedLength, maskedChar, surroundWithParens} = {...defaultMaskedNumberOptions, ...options}
            const str = maskedChar.repeat(maskedLength) + numeric({minLen: unmaskedLength, maxLen: unmaskedLength}).create(seed)
            if (surroundWithParens) {
                return '(' + str + ')'
            }
            return str
        },
        simplify: function*(_seed: number) {}
    }
}

/**
 * Generator for an account number
 * @param length Length of the account number. Default: 15
 */
export function accountNumber(length: number = 15) {
    return numeric({maxLen: length, minLen: length})
}