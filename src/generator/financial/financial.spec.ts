import {
    accountName,
    accountNumber,
    amount,
    bic,
    creditCard,
    currency,
    maskedNumber,
    pin,
    SymbolPos,
    transactionType,
} from './index'
import {range} from "../../core";

describe('Financial', () => {
    it('accountName', () => {
        range(100).forEach(i => expect(accountName().create(i)).toBeTruthy())
    })

    it('bic', () => {
        range(100).forEach(i => expect(bic().create(i)).toMatch(
            /(?<bank>[A-Z]{4})(?<country>[A-Z]{2})(?<location>[A-Z\d]{2})/
        ))
        range(100).forEach(i => expect(bic({includeBranchCode: true}).create(i)).toMatch(
            /(?<bank>[A-Z]{4})(?<country>[A-Z]{2})(?<location>[A-Z\d]{2})(?<branch>[A-Z\d]{3})/
        ))
    })

    it('creditCard', () => {
        range(100).forEach(i => {
            const cc = creditCard.create(i)
            expect(cc.number).toMatch(/^\d+$/)
            expect(cc.cvv).toMatch(/^\d+$/)
            expect(cc.expiration).toMatch(/^\d{2}\/\d{4}$/)
        })
    })

    it('currency', () => {
        range(100).forEach(i => {
            const c = currency.create(i)
            expect(c.isoCode).toMatch(/^[\w\d]+$/)
            expect(c.symbol).not.toEqual("-")
        })
    })

    it('transactionType', () => {
        range(100).forEach(i => {
            expect(transactionType.create(i)).toMatch(/^[\w]+$/)
        })
    })

    it('amount', () => {
        range(100).forEach(i => {
            expect(amount({
                decimalPlaces: 2
            }).create(i)).toMatch(/^\d+\.\d{2}$/)

            expect(amount({
                decimalPlaces: 4
            }).create(i)).toMatch(/^\d+\.\d{4}$/)

            expect(amount({
                symbol: '$',
                symbolPos: SymbolPos.PREFIX,
                decimalPlaces: 2
            }).create(i)).toMatch(/^\$\d+\.\d{2}$/)

            expect(amount({
                symbol: '¢',
                symbolPos: SymbolPos.SUFFIX,
                decimalPlaces: 0
            }).create(i)).toMatch(/^\d+¢$/)
        })
    })

    it('pin', () => {
        range(100).forEach(i => {
            expect(pin(4).create(i)).toMatch(/^\d{4}$/)
        })
        range(100).forEach(i => {
            expect(pin(5).create(i)).toMatch(/^\d{5}$/)
        })
    })

    it('maskedNumber', () => {
        range(100).forEach(i => {
            expect(maskedNumber({
                unmaskedLength: 4,
                maskedLength: 4,
                maskedChar: '*'
            }).create(i)).toMatch(/^\*{4}\d{4}$/)
        })

        range(100).forEach(i => {
            expect(maskedNumber({
                unmaskedLength: 3,
                maskedLength: 5,
                maskedChar: '#'
            }).create(i)).toMatch(/^#{5}\d{3}$/)
        })
    })

    it('account number', () => {
        range(100).forEach(i => {
            expect(accountNumber(14).create(i)).toMatch(/^\d{14}$/)
        })
        range(100).forEach(i => {
            expect(accountNumber(16).create(i)).toMatch(/^\d{16}$/)
        })
    })
})
