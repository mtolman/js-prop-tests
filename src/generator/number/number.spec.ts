import {floats, ints} from "./index";
import {limit, range} from "../../core";

describe('ints', () => {
    it('can generate', () => {
        const gen = ints({min: 10, max: 100})
        range(100).forEach(i => {
            const v = gen.create(i)
            expect(v).toBeLessThanOrEqual(100)
            expect(v).toBeGreaterThanOrEqual(10)
            expect(v >> 0).toEqual(v)
        })
    })

    it('can shrink', () => {
        const gen = ints({min: 10, max: 100})
        expect(gen.create(3)).toBe(13)
        expect([...gen.simplify(3)]).toEqual([12, 11, 10])
    })

    it('can shrink big', () => {
        const gen = ints({min: 10, max: 1000})
        expect(gen.create(300)).toBe(310)
        expect([...limit(9, gen.simplify(300))]).toEqual([160, 85, 48, 29, 20, 15, 13, 12, 11])
    })
})

describe('floats', () => {
    it('can generate', () => {
        const gen = floats({min: 10, max: 100})
        range(100).forEach(i => {
            const v = gen.create(i)
            expect(v).toBeLessThanOrEqual(100)
            expect(v).toBeGreaterThanOrEqual(10)
        })
    })

    it('can shrink', () => {
        const gen = floats({min: 10, max: 100})
        expect(gen.create(3)).toBe(47.516552493329286)
        expect([...limit(5, gen.simplify(3))]).toEqual([
            28.758276246664643,
            19.37913812333232,
            14.68956906166616,
            12.344784530833081,
            11.17239226541654,
        ])
    })
})