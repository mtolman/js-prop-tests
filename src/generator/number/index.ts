import {ValGen} from "../common";
import {randomFloat} from "../../core";

/**
 * Options for generating floating point numbers
 */
export interface FloatOptions {
    /**
     * Minimum number. Defaults to half of minimum safe number range
     */
    min?: number

    /**
     * Maximum number. Defaults to half of maximum number range
     */
    max?: number

    /**
     * Whether to allow NAN values. Default: false
     */
    allowNan?: boolean

    /**
     * Whether to allow infinite values. Default: false
     */
    allowInfinity?: boolean
}

const defaultFpOptions = {
    min: Number.MIN_SAFE_INTEGER / 2,
    max: Number.MAX_SAFE_INTEGER / 2,
    allowNan: false,
    allowInfinity: false
}

/**
 * Options for generating ints
 */
export interface IntOptions {
    /**
     * Minimum number. Defaults to half of minimum safe number range
     */
    min?: number

    /**
     * Maximum number. Defaults to half of maximum number range
     */
    max?: number
}

const defaultIntOptions = {
    min: Number.MIN_SAFE_INTEGER / 2,
    max: Number.MAX_SAFE_INTEGER / 2
}

/**
 * Generates floating point numbers
 * @param options Options for floating point numbers
 */
export function floats(options: FloatOptions = {}) : ValGen<number> {
    const {min, max, allowNan, allowInfinity} = {...defaultFpOptions, ...options}
    const create = (seed: number): number => {
        if (max <= min) {
            return min;
        }

        if (allowNan && seed % 100 == 0) {
            return Number.NaN
        }

        if (allowInfinity && seed % 100 == 1) {
            return Number.POSITIVE_INFINITY
        }
        if (allowInfinity && seed % 100 == 2) {
            return Number.NEGATIVE_INFINITY
        }

        const rnd = randomFloat(seed);

        const diff = (max / 2 - min / 2) * 2;
        return min + diff * rnd();
    }

    return {
        create,
        simplify: function*(seed: number) {
            let val = create(seed)
            const epsilon = 0.0000000001
            while (Number.isFinite(val) && val > min + epsilon && val < max - epsilon) {
                const down = (val + min) / 2
                const up = (val + max) / 2
                if (down > min + epsilon && Math.abs(down) <= Math.abs(up)) {
                    yield down
                    val = down
                }
                else if (up < max - epsilon && Math.abs(up) < Math.abs(down)) {
                    yield up
                    val = up
                }
            }
        }
    }
}

/**
 * Generates integer numbers
 * @param options Options for integer numbers
 */
export function ints(options: IntOptions = {}) : ValGen<number> {
    const {min, max} = {...defaultIntOptions, ...options}
    const create = (seed: number): number => {
        if (max <= min) {
            return min;
        }
        return Math.floor(seed % (max - min) + min);
    }
    return {
        create,
        simplify: function*(seed: number) {
            let val = create(seed)
            while (val > min && val < max) {
                let up, down = 0
                if (Math.abs((max / 2) - (min / 2)) > 50) {
                    down = Math.round((val + min) / 2)
                    up = Math.round((val + max) / 2)
                }
                else {
                    down = val - 1
                    up = val + 1
                }
                if (down >= min && Math.abs(down) <= Math.abs(up)) {
                    yield down
                    val = down
                }
                else if (up <= max && Math.abs(up) < Math.abs(down)) {
                    yield up
                    val = up
                }
                else {
                    return
                }
            }
        }
    }
}
