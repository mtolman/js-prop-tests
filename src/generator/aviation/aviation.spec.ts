import {aircraftTypes, airplanes, airlines, airports} from './index'
import {range} from "../../core";


describe('Aviation', () => {
    it('aircraftTypes', () => {
        range(100).forEach(i => expect(aircraftTypes.create(i)).toMatch(/\w+/))
    })

    it('airplanes', () => {
        range(100).forEach(i => expect(airplanes.create(i).iata).toMatch(/[\w\d]+/))
    })

    it('airlines', () => {
        range(100).forEach(i => expect(airlines.create(i).iata).toMatch(/[\w\d]+/))
    })

    it('airports', () => {
        range(100).forEach(i => expect(airports.create(i).iata).toMatch(/[\w\d]+/))
    })
})
