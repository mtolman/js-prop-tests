import {ValGen} from "../common";
import * as common from '../common';
import aircraftTypesData from './data/aircraft_types'
import airlinesData from './data/airlines'
import airplanesData from './data/airplanes'
import airportsData from './data/airports'

export interface AviationEntry {
    /**
     * The human-readable name (may include unicode characters)
     */
    name: string,
    /**
     * the IATA code
     */
    iata: string,
    /**
     * The ICAO code
     */
    icao: string
}

export interface AviationGenerators {
    /**
     * Generator for types of aircraft (to be used for constants)
     */
    aircraftTypes: ValGen<string>,
    /**
     * Generator for airline information
     */
    airlines: ValGen<AviationEntry>,
    /**
     * Generator for airplane information
     */
    airplanes: ValGen<AviationEntry>,
    /**
     * Generator for airport information
     */
    airports: ValGen<AviationEntry>
}

/**
 * Retrieves versioned aviation generators using a specific data version
 * @param dataVersion Data version to use
 */
export function withVersion(dataVersion: string): AviationGenerators {
    return {
        aircraftTypes: common.index(aircraftTypesData, dataVersion),
        airlines: common.index(airlinesData, dataVersion),
        airplanes: common.index(airplanesData, dataVersion),
        airports: common.index(airportsData, dataVersion),
    }
}

const res = withVersion(common.defaultVersion())

/**
 * Generator for different types of aircraft
 * Meant to be used as a code identifier
 * Uses latest data version
 */
export const aircraftTypes = res.aircraftTypes

/**
 * Generator for different airlines (not exhaustive)
 * Uses latest data version
 */
export const airlines = res.airlines

/**
 * Generator for different airplane models (not exhaustive)
 * Uses latest data version
 */
export const airplanes = res.airplanes

/**
 * Generator for different airports (not exhaustive)
 * Uses latest data version
 */
export const airports = res.airports
