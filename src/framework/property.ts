import {tuple, ValGen} from "../generator/common";
import {shrink} from "./shrink";
import {random} from "../core";

/**
 * Interface for test properties
 */
export interface Property {
    /**
     * Sets the seed to use for the property test
     * @param seed Seed to use for generating random values
     */
    withSeed(seed: number): Property

    /**
     * Sets the generators to use for testing code
     * @param gens Generators to use
     */
    withGens(...gens: ValGen<any>[]): Property

    /**
     * Runs tests on the code to try to find issues
     * @param code Code to run
     */
    test(code: (...params: any[]) => any): void

    /**
     * Sets the iteration to use for the property test. Defaults to 500 iterations
     * @param iters Number of iterations to use when running tests
     */
    withIterations(iters: number): Property
}

const baseSeed = (Math.random() * 3899430923593257328) >>> 0

/**
 * Defines a new property to test which will be tested with generators
 *
 * Example Usage:
 * ```javascript
 * // With generators defined in the same statement
 * property('my prop', ints({min: 10, max: 100}))
 *  .test((a) => expect(myCode(a)).toBeLessThan(9000))
 *
 * // With generators defined in a chain statement
 * property('my prop')
 *  .withGenerators(ints({min: 10, max: 100}))
 *  .test((a) => expect(myCode(a)).toBeLessThan(9000))
 *
 * // With a specific seed (useful for reproducing errors)
 * property('my prop')
 *  .withGens(ints({min: 10, max: 100}))
 *  .withSeed(294839402)
 *  .test((a) => expect(myCode(a)).toBeLessThan(9000))
 *
 * // Can limit how many iterations are performed
 * property('my prop')
 *  .withGenerators(ints({min: 10, max: 100}))
 *  .withIterations(200) // Default is 500 iterations
 *  .test((a) => expect(myCode(a)).toBeLessThan(9000))
 * ```
 *
 * > **Note:** If `withSeed` generated seed can be set for the whole codebase by setting the environment variable PROP_TEST_SEED.
 * > If PROP_TEST_SEED is not set and `withSeed` is not called, then a random seed will be used (from Math.random).
 *
 * @param name Name of the property being checked (used in output on failure)
 * @param gens Generators to use (optional, can also set generators with `withGens`)
 */
export function property(name: string, ...gens: ValGen<any>[]) {
    let seed = baseSeed
    if (process && process.env && process.env.PROP_TEST_SEED) {
        seed = (+process.env.PROP_TEST_SEED) >>> 0
    }
    let ret: any = {}
    let maxIters: number = +(process.env.PROP_TEST_ITERS || 500) || 500
    ret.withSeed = function (newSeed: number) {
        seed = newSeed
        return ret
    }
    ret.withGens = function (...generators: ValGen<any>[]) {
        gens = generators
        return ret
    }
    ret.withIterations = function (iters: number) {
        maxIters = iters
        return ret
    }

    const iteration = (runSeed: number, code: (...params: any[]) => any) => {
        // making a copy so other code won't interfere
        const paramGen = tuple(...gens)
        const params = paramGen.create(runSeed)
        try {
            code(...params)
        }
        catch (e: any) {
            const simplified = shrink(runSeed, paramGen, (params) => {
                try {
                    code(...params)
                    return true
                }
                catch (e) {
                    return false
                }
            })

            const msg = `

Property Failed: ${name}
------------------------------------------------
Seed: ${seed}
Original Input: ${JSON.stringify(params)}
Simplified Input: ${JSON.stringify(simplified)}
------------------------------------------------

`;

            if (typeof e === 'object' && e !== null) {
                if ('matcherResult' in e) {
                    e.matcherResult.input = params;
                    e.matcherResult.simplifiedInput = simplified;
                }
                if ('message' in e) {
                    e.message = msg + e.message
                }
                else if ('stack' in e) {
                    e.stack = msg + e.stack
                }
            }
            else {
                console.error(msg)
            }
            throw e
        }
    }

    ret.test = function(code: (...params: any[]) => any) {
        const rnd = random(seed)
        for (let i = 0; i < maxIters; ++i) {
            iteration(rnd(), code)
        }
    }
    return ret as Property
}