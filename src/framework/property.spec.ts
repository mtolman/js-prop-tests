import {ints} from "../generator/number";
import {property} from "./property";

describe('property check', () => {
    it('handles success cases', () => {
        property('cumulative')
            .withGens(
                ints({min: 1, max: 500}),
                ints({min: 1, max: 500})
            )
            .withSeed(9994938283)
            .test((a: number, b: number) => {
                expect(a + b).toEqual(b + a)
            })
    })

    it('handles error cases', () => {
        try {
            property('cumulative')
                .withGens(
                    ints({min: 1, max: 500}),
                    ints({min: 1, max: 500})
                )
                .withSeed(9994938283)
                .test((a: number, b: number) => {
                    expect(a + b).toEqual(b - a)
                })
            fail('Should throw, above test should fail')
        }
        catch (e: any) {
            expect(e.message).toMatch(/Seed: \d+/gm)
            expect(e.message).toMatch(/Original Input: /gm)
            expect(e.message).toMatch(/Simplified Input: /gm)
            expect(e.message).toMatch(/Property Failed: cumulative/gm)
        }
    })
})