import {ints} from "../generator/number";
import {shrink} from "./shrink";

describe('shrink', () => {
    it('can shrink', () => {
        const gen = ints({min: 1, max: 200})
        const pred = (n: number) => n <= 8
        expect(shrink(123, gen, pred)).toEqual(9)
    })
})