import {limit, random, randomFloat, range, zip} from "./index";

describe('random', function () {
    it('generates values in range', () => {
        const rnd = random(3)
        expect(rnd.min).toEqual(0)
        expect(rnd.max).toEqual(Number.MAX_SAFE_INTEGER >>> 0)
        expect(rnd()).toEqual(1790359622)

        for (let i = 0; i < 1000; ++i) {
            const v = rnd()
            expect(v).toBeGreaterThanOrEqual(rnd.min)
            expect(v).toBeLessThanOrEqual(rnd.max)
        }
    })
});

describe('randomFloat', function () {
    it('generates values in range', () => {
        const rnd = randomFloat(3)
        expect(rnd.min).toEqual(0)
        expect(rnd.max).toEqual(1)

        for (let i = 0; i < 1000; ++i) {
            const v = rnd()
            expect(v).toBeGreaterThanOrEqual(rnd.min)
            expect(v).toBeLessThanOrEqual(rnd.max)
        }
    })
});

describe('range', () => {
    it('range', () => {
        expect(range(10)).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        expect(range(2, 10)).toEqual([2, 3, 4, 5, 6, 7, 8, 9])
        expect(range(10, 2)).toEqual([])
    })
})

describe('zip', () => {
    it('zip', () => {
        expect([...zip([1, 2, 3], ['a', 'b', 'c'], [100, 200])]).toEqual([
            [1, 'a', 100],
            [2, 'b', 200],
            [3, 'c', null]
        ])
    })
})

describe('limit', () => {
    it('limits', () => {
        expect([...limit(10, range(10000))]).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    })
})