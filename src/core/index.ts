

export interface Random {
    (): number;
    min: number;
    max: number;
}

/**
 * Mersenne Twister random number generator
 * Allows generators to be isolated from global state (makes them more reproducible)
 * Generates integers between 0 and 4294967295 (unsigned 32-bit integer max value)
 * @param seed Input seed
 */
export function random (seed: number): Random {
    const stateSize = 624
    const state: number[] = (new Array(624)).fill(0)
    for (const index of state.keys()) {
        if (index === 0) {
            state[index] = seed
        }
        else {
            // Bitshift at the end converts it to an unsigned int
            state[index] = (1812433253 * (state[index-1] ^ (state[index-1] >> 30)) + index) >>> 0
        }
    }
    let next = 0

    function twist() {
        const M = 397
        const firstHalf = stateSize - M
        let index = 0

        for (; index < firstHalf; ++index) {
            const bits = ((state[index] & 0x80000000) | (state[index + 1] & 0x7fffffff)) >>> 0
            state[index] = (state[index + M] ^ (bits >> 1) ^ ((bits & 1) * 0x9908b0df)) >>> 0
        }
        for ( ; index < stateSize - 1; index++)
        {
            const bits = ((state[index] & 0x80000000) | (state[index + 1] & 0x7fffffff)) >>> 0
            state[index] = (state[index - firstHalf] ^ (bits >> 1) ^ ((bits & 1) * 0x9908b0df)) >>> 0
        }

        const  bits = ((state[index] & 0x80000000) | (state[0] & 0x7fffffff)) >>> 0
        state[index] = (state[M - 1] ^ (bits >> 1) ^ ((bits & 1) * 0x9908b0df)) >>> 0
        next = 0;
    }

    twist()

    const res: Random = function() {
        if (next >= stateSize) {
            twist()
        }
        let x: number = state[next++]
        x ^= x >> 1
        x ^= (x <<  7) & 0x9d2c5680
        x ^= (x << 15) & 0xefc60000
        x ^=  x >> 18
        return x
    } as any

    res.min = 0
    // Bitshift at the end converts it to an unsigned int
    res.max = Number.MAX_SAFE_INTEGER >>> 0

    return res
}

/**
 Mersenne Twister random number generator
 Allows generators to be isolated from global state (makes them more reproducible)
 Generates floating point numbers between 0 and 1
 * @param seed
 */
export function randomFloat(seed: number) : Random {
    const rnd = random(seed)
    const res : Random = function() {
        return rnd() / rnd.max
    } as any
    res.min = 0
    res.max = 1.0
    return res
}

/**
 * Returns a range of number
 * @param fromOrTo If `to` is not provided, the (exclusive) end. If `to` is provided, the (inclusive) start
 * @param to If provided, the (exclusive) end. If not provided, the range will start at 0
 */
export function range(fromOrTo: number, to?: number) : number[] {
    if (typeof to === "undefined") {
        return range(0, fromOrTo)
    }
    else if (fromOrTo < to) {
        return [...(new Array(to - fromOrTo)).keys()].map(i => i + fromOrTo)
    }
    else {
        return []
    }
}


export function hexStr(byte: number) {
    byte |= 0
    const strs = "0123456789ABCDEF"
    const lower = byte & 0b1111
    const upper = (byte >> 4) & 0b1111
    return `${strs[upper]}${strs[lower]}`
}

export function* zip(...arrs: Iterable<any>[]) {
    const iters: (Iterator<any>|null)[] = arrs.map(iter => iter[Symbol.iterator]())
    const values = (new Array(arrs.length)).fill(null)
    while(true) {
        let oneHadValue = false
        for (let a = 0; a < iters.length; ++a) {
            const iter = iters[a]
            if (iter === null) {
                continue
            }
            const res = iter.next()
            if (res.done) {
                iters[a] = null
                values[a] = null
            }
            else {
                values[a] = res.value
                oneHadValue = true
            }
        }
        if (oneHadValue) {
            yield [...values]
        }
        else {
            return
        }
    }
}


export function* limit<T>(limit: number, iter: Iterable<T>) {
    for (const v of iter) {
        yield v
        if (--limit == 0) {
            return
        }
    }
}
